/* 
SHA-1 in C   By Steve Reid <steve@edmweb.com>   100% Public Domain 
 
Test Vectors (from FIPS PUB 180-1) 
"abc" 
  A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D 
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" 
  84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1 
A million repetitions of "a" 
  34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F 
*/

#ifndef __SHA1_H__
#define __SHA1_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* 以下两个 宏必须根据自己情况定义 */
#ifndef SHA1HANDSOFF
#define SHA1HANDSOFF
#endif
#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN
#endif

/* Exported types ------------------------------------------------------------*/
typedef struct
{
    unsigned long state[5];         /* 160(5×32)比特的消息摘要（即SHA-1算法要得出的） */
    unsigned long count[2];         /* 储存消息的长度（单位：比特） */
    unsigned char buffer[64];       /* 512(64×8)比特（位）的消息块（由原始消息经处理得出） */
} SHA1_CTX;


/* Exported constants --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void SHA1Init(SHA1_CTX *context);

void SHA1Update(SHA1_CTX *context, unsigned char *data, unsigned int len);

void SHA1Final(unsigned char digest[20], SHA1_CTX *context);

#ifdef __cplusplus
}
#endif

#endif      /* __MD5_H__ */

