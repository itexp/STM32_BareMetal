#ifndef __MD5_H__
#define __MD5_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* MD5 context. */
typedef struct
{
    /* number of bits, modulo 2^64 (lsb first) */  
    /*存储原始信息的bits数长度,不包括填充的bits，最长为 2^64 bits，因为2^64是一个64位数的最大值*/
    unsigned int count[2];
    /* state (ABCD) */
    /*四个32bits数，用于存放最终计算得到的消息摘要。当消息长度〉512bits时，也用于存放每个512bits的中间结果*/ 
    unsigned int state[4];
    /* input buffer */
    /*存放输入的信息的缓冲区，512bits*/
    unsigned char buffer[64];
} MD5_CTX;

/* Exported constants --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void MD5Init(MD5_CTX *context);

void MD5Update(MD5_CTX *context, unsigned char *input, unsigned int inputlen);

void MD5Final(MD5_CTX *context, unsigned char digest[16]);

#ifdef __cplusplus
}
#endif

#endif      /* __MD5_H__ */

