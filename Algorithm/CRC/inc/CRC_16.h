#ifndef __SHA1_H__
#define __SHA1_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
unsigned short int CRC16GetValue(unsigned char* pData, unsigned short int length, unsigned short int crc);

#ifdef __cplusplus
}
#endif

#endif      /* __MD5_H__ */

