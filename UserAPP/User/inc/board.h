/**
  ******************************************************************************
  * @file    board.h
  * @author  ZCShou
  * @version 1.4.0
  * @date    2020.4.8
  * @brief   开发板资源初始化
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BOARD_H__
#define __BOARD_H__

/* Includes ------------------------------------------------------------------*/
#include "BaseDef.h"
#include "ConfigMCU.h"
/* Exported define -----------------------------------------------------------*/
#ifndef NVIC_PRIORITYGROUP_0
    #define NVIC_PRIORITYGROUP_0    ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                4 bits for subpriority */
    #define NVIC_PRIORITYGROUP_1    ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                3 bits for subpriority */
    #define NVIC_PRIORITYGROUP_2    ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                2 bits for subpriority */
    #define NVIC_PRIORITYGROUP_3    ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                1 bit  for subpriority */
    #define NVIC_PRIORITYGROUP_4    ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                0 bit  for subpriority */
#endif
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void BoardInit(void);

#endif /* __BOARD_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
