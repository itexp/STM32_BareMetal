/**
  ******************************************************************************
  * @file    board.h
  * @author  ZCShou
  * @version 1.4.0
  * @date    2020.4.8
  * @brief   开发板资源初始化
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "board.h"
#include "stm32f7xx_ll_bus.h"
#include "stm32f7xx_ll_system.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_cortex.h"
#include "stm32f7xx_ll_utils.h"
#include "stm32f7xx_ll_pwr.h"
#include "UART.h"
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
 * @brief System Clock Configuration
 * @retval None
**/
static void SystemClock_Config(void)
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
    while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_4)
    {
    }
    LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE3);
    LL_PWR_DisableOverDriveMode();
    LL_RCC_HSE_Enable();

    /* Wait till HSE is ready */
    while(LL_RCC_HSE_IsReady() != 1)
    {

    }
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_25, 256, LL_RCC_PLLP_DIV_2);
    LL_RCC_PLL_Enable();

    /* Wait till PLL is ready */
    while(LL_RCC_PLL_IsReady() != 1)
    {

    }
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_4);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_2);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

    /* Wait till System clock is ready */
    while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
    {

    }
    LL_Init1msTick(128000000);
    LL_SetSystemCoreClock(128000000);

    LL_SYSTICK_EnableIT();
}

/**
  * @brief MPU Configuration
  * @retval None
  */
static void MPU_Config(void)
{
    /* Disables the MPU */
    LL_MPU_Disable();

    /** Initializes and configures the Region and the memory to be protected
     */
    LL_MPU_ConfigRegion( LL_MPU_REGION_NUMBER0, 0x87, 0x0,
            LL_MPU_REGION_SIZE_4GB | LL_MPU_TEX_LEVEL0 | LL_MPU_REGION_NO_ACCESS |
            LL_MPU_INSTRUCTION_ACCESS_DISABLE | LL_MPU_ACCESS_SHAREABLE |
            LL_MPU_ACCESS_NOT_CACHEABLE | LL_MPU_ACCESS_NOT_BUFFERABLE);
    /* Enables the MPU */
    LL_MPU_Enable(LL_MPU_CTRL_PRIVILEGED_DEFAULT);
}

/** BoardInit
 *
 * @param[in]   void
 * @retval      NULL
**/
void BoardInit(void)
{
    MPU_Config();

    SystemClock_Config();

    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);

    /* System interrupt init*/
    NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    /* SysTick_IRQn interrupt configuration */
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 15, 0));

    // UART_Init(UART_COMM, UART_BAUDRATE_115200, UART_DATA_BIT_8, UART_PARITY_BIT_NULL, UART_STOP_BIT_1);
}

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/

