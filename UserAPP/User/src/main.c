/**
  ******************************************************************************
  * @file    main.c
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.3.18
  * @brief   程序入口文件
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2018 ZCShou </center></h2>
  *
  * 1. 不带 IAP 的项目模板！
  *
  * 2. 整个程序的基本组成结构（通用性设计，目前以下的程序设计理论上适合所有ST芯片，只需要根据产品修改各宏值即可）
  *        (+) APP：主应用程序，正常的功能操作均在此。
  *        (+) PRAM：在 Flash 的一块独立存储空间，用来存放各种参数。
  *        注意：
  *         (1) 可能需要根据芯片的FLash（扇区大小不一致），调整参数区和APP的位置
  *
  * 3. APP 结构均为：ARM中断向量表 + 自己添加的程序信息 + ARM程序代码。配合分散加载文件实现！配合分散加载文件实现！配合分散加载文件实现！
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "main.h"
// #include "UART.h"
#include "board.h"
#include "Delay.h"
#include "retarget.h"
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/**
 * @brief  应用程序信息(需要连接器脚本文件放到指定位置)
 */
#if defined ( __ICCARM__ )
__root const APP_INFO app_info @ ".app_info" = {
#else
const APP_INFO app_info __attribute__((section(".app_info")))= {
#endif
    /* 厂商标识 */
    FACTORY,
    /* 产品标识*/
    PRODUCT,
    /* 规约标识 */
    PROTOCOL,
    /* 软件版本 */
    VER_SW,
    /* 软件版本发布日期 */
    DATE_SW,
    /* 硬件版本 */
    VER_HW,
    /* 硬件版本发布日期 */
    DATE_HW,
};

/* Private functions ---------------------------------------------------------*/
/** 主程序
 *
 * @param[in]   void
 * @retval      NULL
**/
int main (void)
{
    int x = 0;

    BoardInit();

    RETARGET_init();

    while(1)
    {
        printf("Please input a value: ");
        scanf("%d", &x);
        printf("\r\nInput = %d\r\n", x);
    }
}

/************************ (C) COPYRIGHT ZCShou ***********END OF FILE**********/
