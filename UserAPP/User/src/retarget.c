/**
  ******************************************************************************
  * @file    retarget.c
  * @author  ZCShou
  * @version 1.4.0
  * @date    2020.4.8
  * @brief   重定向标准输出输出
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "retarget.h"
#if RETARGET_NO_SEMIHOSTING == 1
#include "stm32f7xx_ll_system.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_bus.h"
#include "stm32f7xx_ll_utils.h"
#include "stm32f7xx_ll_gpio.h"
#include "stm32f7xx_ll_usart.h"
/* Private define ------------------------------------------------------------*/
#define UARTx                       USART1
#define UART_CLK_ENABLE()           LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1)
#define UART_CLK_DISABLE()          LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_USART1)
#define UART_TX_PIN                 LL_GPIO_PIN_9
#define UART_TX_GPIOx               GPIOA
#define UART_TX_GPIO_CLK            LL_AHB1_GRP1_PERIPH_GPIOA
#define UART_RX_PIN                 LL_GPIO_PIN_10
#define UART_RX_GPIOx               GPIOA
#define UART_RX_GPIO_CLK            LL_AHB1_GRP1_PERIPH_GPIOA
#define UART_GPIO_CLK_ENABLE()      LL_AHB1_GRP1_EnableClock(UART_TX_GPIO_CLK | UART_RX_GPIO_CLK)
#define UART_BAUDRATE               115200
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
#if defined ( __GNUC__ ) && !defined(__SEGGER_CC__)
extern void initialise_monitor_handles(void);
#endif

/** 串口使用的GPIO配置
 *
 * @param[in] None
 * @retval NULL
**/
static void STM32F7UART_GPIO_Config(void)
{
    LL_GPIO_InitTypeDef LL_GPIO_Struct = {0};

    LL_GPIO_StructInit(&LL_GPIO_Struct);
    LL_GPIO_Struct.Mode = LL_GPIO_MODE_ALTERNATE;
    LL_GPIO_Struct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Struct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    LL_GPIO_Struct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    LL_GPIO_Struct.Alternate = LL_GPIO_AF_7;
    UART_GPIO_CLK_ENABLE();
    LL_GPIO_Struct.Pin = UART_TX_PIN;
    LL_GPIO_Init(UART_TX_GPIOx, &LL_GPIO_Struct);
    LL_GPIO_Struct.Pin = UART_RX_PIN;
    LL_GPIO_Init(UART_TX_GPIOx, &LL_GPIO_Struct);
}

/** 串口配置
 *
 * @param[in] None
 * @retval NULL
**/
static void STM32F7UART_Config(void)
{
    LL_USART_InitTypeDef UART_InitStruct = {0};

    UART_InitStruct.BaudRate = UART_BAUDRATE;
    UART_InitStruct.Parity = LL_USART_PARITY_NONE;
    UART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    UART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    UART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    UART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    LL_USART_DeInit(UARTx);        /* 先反初始化，防止多次调用初始化时出错 */
    UART_CLK_ENABLE();
    LL_USART_Init(UARTx, &UART_InitStruct);
    LL_USART_Enable(UARTx);
}

/**
 * @brief RETARGET_init
 * @param[in] None
 * @retval None
**/
void RETARGET_init(void)
{
    STM32F7UART_GPIO_Config();

    STM32F7UART_Config();

    #if defined ( __GNUC__ ) && !defined(__SEGGER_CC__)
    initialise_monitor_handles();
    #endif
}

#if defined ( __CC_ARM )
#if defined (__MICROLIB)

static int getchar_undo =  0;
static int getchar_ch   = -1;

/**
 * @brief fputc
 * @param[in] ch
 * @param[in] stream
 * @retval None
**/
int fputc(int ch, FILE *stream)
{
    while (LL_USART_IsActiveFlag_TC(UARTx) == RESET);
    LL_USART_TransmitData8(UARTx, (uint8_t) ch);
    
    return ch;
}

/**
 * @brief fgetc
 * @param[in] stream
 * @retval None
**/
int fgetc(FILE *stream)
{
    int ch;

    if (getchar_undo)
    {
        ch = getchar_ch;
        getchar_ch = -1;
        getchar_undo = 0;
    }
    else
    {
        while (LL_USART_IsActiveFlag_RXNE(UARTx) == RESET);
        ch = LL_USART_ReceiveData8(UARTx);
        getchar_ch = ch;
    }

    return ch;
}

/**
   The function __backspace() is used by the scanf family of functions, and must
   be re-implemented if you retarget the stdio arrangements at the fgetc() level.

  \param[in] stream  Stream handle

  \return    The value returned by __backspace() is either 0 (success) or EOF
             (failure). It returns EOF only if used incorrectly, for example,
             if no characters have been read from the stream. When used
             correctly, __backspace() must always return 0, because the scanf
             family of functions do not check the error return.
*/
int __backspace(FILE* stream)
{
    if (stream == &__stdin)
    {
        if (getchar_ch != -1)
        {
            getchar_undo = 1;
            return 0;
        }
    }

    return -1;
}

#else /* __MICROLIB */
#include <rt_misc.h>
#include <rt_sys.h>
#include <time.h>

/* disable semihosting */
#if __ARMCC_VERSION >= 6000000
__asm(".global __use_no_semihosting");
#elif __ARMCC_VERSION >= 5000000
#pragma import(__use_no_semihosting)
#else
#pragma import(__use_no_semihosting_swi)
#endif

/* IO device file handles. */
#define FH_STDIN  0x8001
#define FH_STDOUT 0x8002
#define FH_STDERR 0x8003

/* Standard IO device name defines. */
const char __stdin_name[]  = ":STDIN";
const char __stdout_name[] = ":STDOUT";
const char __stderr_name[] = ":STDERR";

/**
  Defined in rt_sys.h, this function opens a file.

  The _sys_open() function is required by fopen() and freopen(). These
  functions in turn are required if any file input/output function is to
  be used.
  The openmode parameter is a bitmap whose bits mostly correspond directly to
  the ISO mode specification. Target-dependent extensions are possible, but
  freopen() must also be extended.

  \param[in] name     File name
  \param[in] openmode Mode specification bitmap

  \return    The return value is -1 if an error occurs.
*/
FILEHANDLE _sys_open(const char* name, int openmode)
{
    (void)openmode;

    if (name == NULL)
    {
        return (-1);
    }

    if (name[0] == ':')
    {
        if (strcmp(name, ":STDIN") == 0)
        {
            return (FH_STDIN);
        }
        if (strcmp(name, ":STDOUT") == 0)
        {
            return (FH_STDOUT);
        }
        if (strcmp(name, ":STDERR") == 0)
        {
            return (FH_STDERR);
        }
        return (-1);
    }

    return (-1);
}

/**
  Defined in rt_sys.h, this function closes a file previously opened
  with _sys_open().

  This function must be defined if any input/output function is to be used.

  \param[in] fh File handle

  \return    The return value is 0 if successful. A nonzero value indicates
             an error.
*/
int _sys_close(FILEHANDLE fh)
{
    switch (fh)
    {
        case FH_STDIN:
            return (0);
        case FH_STDOUT:
            return (0);
        case FH_STDERR:
            return (0);
    }

    return (-1);
}

int _sys_write(FILEHANDLE fh, const unsigned char* buf, unsigned len, int mode)
{
    for (; len; len--)
    {
        while (LL_USART_IsActiveFlag_TC(UARTx) == RESET);
        LL_USART_TransmitData8(UARTx, *buf);
        buf++;
    }

    return 0;
}

int _sys_read(FILEHANDLE fh, unsigned char* buf, unsigned len, int mode)
{
    uint8_t ch = 0;

    while (LL_USART_IsActiveFlag_TC(UARTx) == RESET);
    ch = LL_USART_ReceiveData8(UARTx);
    *buf++ = ch;
    len--;
    return ((int)(len));
}

void _ttywrch(int ch) {}

int _sys_istty(FILEHANDLE fh)
{
    return 0;
}

int _sys_seek(FILEHANDLE fh, long pos)
{
    return -1;
}

long _sys_flen(FILEHANDLE fh)
{
    return -1;
}

void _sys_exit(int return_code)
{
    while (1)
        ;
}
#endif /* __MICROLIB */
#endif  /* __CC_ARM */

#if defined ( __ICCARM__ )
/**
 * @brief __read
 * @param[in] handle
 * @param[in] buf
 * @param[in] bufSize
 * @retval None
**/
size_t __read(int handle, unsigned char *buf, size_t bufSize)
{
    if (handle == 0)
    {
        size_t nChars = 0;
        for (size_t i = bufSize; i > 0; --i)
        {
            while (LL_USART_IsActiveFlag_RXNE(UARTx) == RESET);
            const int c = LL_USART_ReceiveData8(UARTx);
            if (c < 0)
            {
                break;
            }
            *buf++ = (unsigned char)c;
            nChars++;
        }
        return nChars;
    }

    return -1;
}

/**
 * @brief __write
 * @param[in] handle
 * @param[in] buf
 * @param[in] bufSize
 * @retval None
**/
size_t __write(int handle, const unsigned char* buf, size_t bufSize)
{
    /* Check for the command to flush all handles */
    if (handle == -1)
    {
        return 0;
    }

    if (handle == 1 /* stdout */ || handle == 2 /* stderr */)
    {
        for (size_t i = bufSize; i > 0; --i)
        {
            while (LL_USART_IsActiveFlag_TC(UARTx) == RESET);
            LL_USART_TransmitData8(UARTx, *buf);
            buf++;
        }
        return bufSize;
    }
    return -1;
}
#endif  /* __ICCARM__ */

#if defined ( __GNUC__ ) && !defined(__SEGGER_CC__)
#include <sys/stat.h>
#include <sys/types.h>

int _open (const char *path, int oflag, int mode)
{
    return -1;
}

int _close (int fildes)
{
    return 0;
}
ssize_t _write (int fildes, const void *buf, size_t nbyte)
{
    /* Write "len" of char from "ptr" to file id "fd"
     * Return number of char written.
     * Need implementing with UART here. */
    return nbyte;
}

ssize_t _read (int fildes, void *buf, size_t nbyte)
{
    /* Read "len" of char to "ptr" from file id "fd"
     * Return number of char read.
     * Need implementing with UART here. */
    return nbyte;
}

int _isatty (int fildes)
{
    /* Write one char "ch" to the default console
     * Need implementing with UART here. */
    return 1;
}

off_t _lseek (int fildes, off_t offset, int whence)
{
    return (-1);
}

int _fstat (int fildes, struct stat *buf)
{
    return (0);
}

pid_t _getpid(void)
{
    return (pid_t)1;
}

int _kill(int pid, int sig)
{
    return 0;
}

#endif  /* __GNUC__ */

#else   /* RETARGET_NO_SEMIHOSTING == 1 */
/**
 * @brief RETARGET_init
 * @param[in] None
 * @retval None
**/
void RETARGET_init(void)
{
}
#endif  /* RETARGET_NO_SEMIHOSTING == 1 */
/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/

