/*
****************************************************************
** *** Linker Script File by ZCShou ***
** *************************************************************
**     Name           Address              Size              Discription
**   ITCM_RAM       0x00000000          0x00004000
**   ITCM_FLASH     0x00200000          0x00200000   FLASH has only one but interface is different.(default booting address when BOOT_PIN = 0)
**   FLASH          0x08000000          0x00200000   FLASH has only one but interface is different.
**   RAM            0x20020000          0x00060000
**   DTCM_RAM       0x20000000          0x00020000
** *************************************************************
*/

define memory with size = 4G;

//
// Combined regions per memory type
//
define region FLASH = RAM1;
define region INST_RAM = DTCM_RAM1;
define region DATA_RAM = DTCM_RAM1;

//
// Block definitions
//
define block vectors                        { section .vectors };                                   // Vector table section
define block vectors_ram                    { section .vectors_ram };                               // Vector table section
define block ctors                          { section .ctors,     section .ctors.*, block with         alphabetical order { init_array } };
define block dtors                          { section .dtors,     section .dtors.*, block with reverse alphabetical order { fini_array } };
define block exidx                          { section .ARM.exidx, section .ARM.exidx.* };
define block tbss                           { section .tbss,      section .tbss.*  };
define block tdata                          { section .tdata,     section .tdata.* };
define block tls with fixed order           { block tbss, block tdata };
define block tdata_load                     { copy of block tdata };
define block heap  with auto size = __HEAPSIZE__,  alignment = 8, readwrite access { };
define block stack with      size = __STACKSIZE__, alignment = 8, readwrite access { };

//
// Explicit initialization settings for sections
// Packing options for initialize by copy: packing=auto/lzss/zpak/packbits
//
do not initialize                           { section .non_init, section .non_init.*, section .*.non_init, section .*.non_init.* };
do not initialize                           { section .no_init, section .no_init.*, section .*.no_init, section .*.no_init.* };   // Legacy sections, kept for backwards compatibility
do not initialize                           { section .noinit, section .noinit.*, section .*.noinit, section .*.noinit.* };       // Legacy sections, used by some SDKs/HALs
do not initialize                           { block vectors_ram };
initialize by copy with packing=auto        { section .data, section .data.*, section .*.data, section .*.data.* };               // Static data sections
initialize by copy with packing=auto        { section .fast, section .fast.*, section .*.fast, section .*.fast.* };               // "RAM Code" sections

#define USES_ALLOC_FUNC                                   \
  linked symbol malloc || linked symbol aligned_alloc ||  \
  linked symbol calloc || linked symbol realloc

initialize by calling __SEGGER_init_heap if USES_ALLOC_FUNC { block heap };                        // Init the heap if one is required
initialize by calling __SEGGER_init_ctors    { block ctors };                                      // Call constructors for global objects which need to be constructed before reaching main (if any). Make sure this is done after setting up heap.

//assert with warning "free() linked into application but there are no calls to an allocation function!" {
//  linked symbol free => USES_ALLOC_FUNC
//};

assert with error "heap is too small!"              { USES_ALLOC_FUNC => size  of block heap >= 48 };
assert with error "heap size not a multiple of 8!"  { USES_ALLOC_FUNC => size  of block heap % 8 == 0 };
assert with error "heap not correctly aligned!"     { USES_ALLOC_FUNC => start of block heap % 8 == 0 };

//
// Explicit placement in FLASHn
//
place in FLASH1                             { section .FLASH1, section .FLASH1.* };
place in ITCM_FLASH1                        { section .ITCM_FLASH1, section .ITCM_FLASH1.* };  // This region may be deactivated and not accessible
//
// FLASH Placement
//
place at start of FLASH                     { block vectors };                                      // Vector table section
place in FLASH with minimum size order      { block tdata_load,                                     // Thread-local-storage load image
                                              block exidx,                                          // ARM exception unwinding block
                                              block ctors,                                          // Constructors block
                                              block dtors,                                          // Destructors block
                                              readonly,                                             // Catch-all for readonly data (e.g. .rodata, .srodata)
                                              readexec                                              // Catch-all for (readonly) executable code (e.g. .text)
                                            };

//
// Explicit placement in RAMn
//
place in DTCM_RAM1                          { section .DTCM_RAM1, section .DTCM_RAM1.* };
place in ITCM_RAM1                          { section .ITCM_RAM1, section .ITCM_RAM1.* };  // This region may be deactivated and not accessible
place in RAM1                               { section .RAM1, section .RAM1.* };
//
// RAM Placement
//
place at start of DATA_RAM                   { block vectors_ram };
place in INST_RAM                            { section .fast, section .fast.* };                    // "ramfunc" section
place in DATA_RAM with auto order            { block tls,                                           // Thread-local-storage block
                                              readwrite,                                            // Catch-all for initialized/uninitialized data sections (e.g. .data, .noinit)
                                              zeroinit                                              // Catch-all for zero-initialized data sections (e.g. .bss)
                                            };
place in DATA_RAM                            { block heap };                                        // Heap reserved block
place at end of DATA_RAM                     { block stack };                                       // Stack reserved block at the end
