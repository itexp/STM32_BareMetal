/**
  ******************************************************************************
  * @file    Test.h
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   Test 
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2019 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
#ifndef __TEST_H__
#define __TEST_H__

#ifdef __cplusplus
 extern "C" { 
#endif
/* Includes ------------------------------------------------------------------*/
#include "ConfigAPP.h"
#include "ConfigDrivers.h"
#if DEBUG_MODE == 1
/* Exported defines ----------------------------------------------------------*/
#warning "MUST FORBIDDEN ALL TEST WHEN RELEASING"
/* Exported types ------------------------------------------------------------*/
/* Exported variables ------------------------------------------------------- */
/* Exported functions ------------------------------------------------------- */
void TestProcess(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __TEST_LCD_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
