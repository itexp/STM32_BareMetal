/**
  ******************************************************************************
  * @file    Test.c
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   Test 
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2019 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "Test.h"
#if DEBUG_MODE == 1
#include "TestLCD.h"
#include "TestRTC.h"
#include "TestKEY.h"
#include "TestPTL.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** 初始化
 *
 * @param[in] void
 * @retval NULL
**/
void TestProcess(void)
{
    // Test_LCD();

    // Test_RTC();

    // Test_KEY();

    Test_PTL();

}

/**
  * @}
  */
#endif
/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
