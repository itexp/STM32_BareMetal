/**
  ******************************************************************************
  * @file    DelayTimers_STM32F7_LL.h 
  * @author  赵长收
  * @version 1.0.0
  * @date    2019.05.17
  * @brief   定时器
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2018 iESlab </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_TIMERS_STM32F7_LL_H__
#define __DELAY_TIMERS_STM32F7_LL_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "Delay.h"   /* 接口文件定义了对硬件的抽象 */
#if DELAY_TIMERS_ENABLE == 1
/* Exported define -----------------------------------------------------------*/
/* 配置项检查 */
#if  DELAY_TIMERS_TIMER_APB != 1 && DELAY_TIMERS_TIMER_APB != 2
#error "Error!"
#endif
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
bool STM32F7TIMER_Init(void);

void STM32F7TIMER_DelayUS(unsigned int us);

#endif

#ifdef __cplusplus
}
#endif

#endif /* __DELAY_TIMERS_STM32F7_LL_H__ */

/************************ (C) COPYRIGHT iESlab *****END OF FILE****/
