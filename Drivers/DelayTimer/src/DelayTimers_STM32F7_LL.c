/**
  ******************************************************************************
  * @file    DelayTimers_STM32F7_LL.c 
  * @author  赵长收
  * @version 1.0.0
  * @date    2019.05.17
  * @brief   定时器
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2018 iESlab </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "../inc/DelayTimers_STM32F7_LL.h"
#if DELAY_TIMERS_ENABLE == 1
#include "stm32f7xx_ll_tim.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_bus.h"
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#ifndef DELAY_TIMERS_FEED_WEG
#define DELAY_TIMERS_FEED_WEG()
#endif
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup Timers Config functions
 *  @brief   x
 *  
@verbatim   
 ===============================================================================
               ##### Timers Config functions #####
 ===============================================================================
    [..] 
    [..] 
    [..] 
@endverbatim
  * @{
  */

/**
 * @brief 初始化
 * @param[in] void
 * @retval  是否初始化成功
 */
bool STM32F7TIMER_Init(void)
{
    LL_TIM_InitTypeDef TIM_InitStruct;
    LL_RCC_ClocksTypeDef RCC_Clocks;
    unsigned int Freq = 0;

    /* 时钟 */
    DELAY_TIMERS_TIMER_CLK_ENABLE();

    /* 读取总线时钟，用来计算重装值 */
    LL_RCC_GetSystemClocksFreq(&RCC_Clocks);
    #if DELAY_TIMERS_TIMER_APB == 1
    Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK1_Frequency > 1 ? RCC_Clocks.PCLK1_Frequency * 2 : RCC_Clocks.PCLK1_Frequency;
    #else
    Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK2_Frequency > 1 ? RCC_Clocks.PCLK2_Frequency * 2 : RCC_Clocks.PCLK2_Frequency;
    #endif

    /* 参数 */
    TIM_InitStruct.Prescaler = Freq/1000000 - 1;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_DOWN;
    TIM_InitStruct.Autoreload = 0xFFFF;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;
    
    LL_TIM_Init(DELAY_TIMERS_TIMER, &TIM_InitStruct);

    while ((DELAY_TIMERS_TIMER->SR & LL_TIM_SR_UIF)!=SET)
    {
        DELAY_TIMERS_FEED_WEG();
    }
    DELAY_TIMERS_TIMER->SR = (uint16_t)~LL_TIM_SR_UIF;

    return true;
}

/**
  * @}
  */


/** @defgroup Delay
 *  @brief   延时
 *  
@verbatim   
 ===============================================================================
               ##### 延时 #####
 ===============================================================================
    [..] 
    [..] 
@endverbatim
  * @{
  */

/**
 * @brief 微妙延时
 * @param[in] us        要延时的微妙数
 * @retval  void
 */
void STM32F7TIMER_DelayUS(unsigned int us)
{
    DELAY_TIMERS_TIMER->CR1 &= ~TIM_CR1_CEN;
    DELAY_TIMERS_TIMER->CNT = us - 1;
    DELAY_TIMERS_TIMER->CR1 |= TIM_CR1_CEN;
    while ((DELAY_TIMERS_TIMER->SR & LL_TIM_SR_UIF) != SET)
    {
        DELAY_TIMERS_FEED_WEG();
    }
    DELAY_TIMERS_TIMER->SR = (uint16_t)~LL_TIM_SR_UIF;
    DELAY_TIMERS_TIMER->CR1 &= ~TIM_CR1_CEN;
}

/**
  * @}
  */
#endif

/************************ (C) COPYRIGHT iESLab *****END OF FILE****/
