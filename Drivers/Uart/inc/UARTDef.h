/**
  ******************************************************************************
  * @file    UARTDef.h
  * @author  ZCShou
  * @version 1.4.0
  * @date    2020.4.8
  * @brief   UART 通用数据项的定义
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1) 通过通用数据项以统一不同时钟芯片之前区别
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
#ifndef __UART_DEF_H__
#define __UART_DEF_H__

#ifdef __cplusplus
 extern "C" { 
#endif
/* Includes ------------------------------------------------------------------*/
#include "ConfigDrivers.h"
/* Exported defines ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/** 
  * @brief 波特率枚举
  */
typedef enum UART_BAUDRATE_TYPE
{
    UART_BAUDRATE_1200 = 1200,
    UART_BAUDRATE_2400 = 2400,
    UART_BAUDRATE_4800 = 4800,
    UART_BAUDRATE_9600 = 9600,
    UART_BAUDRATE_19200 = 19200,
    UART_BAUDRATE_38400 = 38400,
    UART_BAUDRATE_57600 = 57600,
    UART_BAUDRATE_115200 = 115200
} UART_BAUDRATE_TYPE;

/** 
  * @brief 波特率索引枚举（必须不能与 UART_BAUDRATE_TYPE 重复）
  */
typedef enum UART_BAUDRATE_INDEX_TYPE
{
    UART_BAUDRATE_INDEX_1200 = 1,
    UART_BAUDRATE_INDEX_2400 = 2,
    UART_BAUDRATE_INDEX_4800 = 3,
    UART_BAUDRATE_INDEX_9600 = 4,
    UART_BAUDRATE_INDEX_19200 = 5,
    UART_BAUDRATE_INDEX_38400 = 6,
    UART_BAUDRATE_INDEX_57600 = 7,
    UART_BAUDRATE_INDEX_115200 = 8
} UART_BAUDRATE_INDEX_TYPE;

/** 
  * @brief 数据位数枚举
  */
typedef enum UART_DATA_BIT_TYPE
{
    UART_DATA_BIT_8 = 0,            /*  */
    UART_DATA_BIT_9 = 1
} UART_DATA_BIT_TYPE;

/** 
  * @brief 校验位枚举
  */
typedef enum UART_PARITY_BIT_TYPE
{
    UART_PARITY_BIT_NULL = 0,        /*  */
    UART_PARITY_BIT_ODD = 1,
    UART_PARITY_BIT_EVEN = 2
} UART_PARITY_BIT_TYPE;

/** 
  * @brief 停止位枚举
  */
typedef enum UART_STOP_BIT_TYPE
{
    UART_STOP_BIT_1 = 0,            /*  */
    UART_STOP_BIT_2 = 1
} UART_STOP_BIT_TYPE;
/* Exported variables ------------------------------------------------------- */
/* Exported macro ----------------------------------------------------------- */
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif /* __UART_DEF_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
