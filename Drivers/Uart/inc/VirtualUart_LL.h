/**
  ******************************************************************************
  * @file    VirtualUart_LL.h
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   模拟串口驱动实现源文件（基于 LL 库）
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
 
#ifndef __VIRTUAL_UART_LL_H__
#define __VIRTUAL_UART_LL_H__

#ifdef __cplusplus
 extern "C" { 
#endif
     
/* Includes ------------------------------------------------------------------*/
#include "UARTDef.h"
#if VIRTUAL_UART1 == 1 || VIRTUAL_UART2 == 2 || VIRTUAL_UART3 == 3
/* Exported defines ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void VirtualUartInit(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop);

void VirtualUartDeInit(UART_TYPE eCom);

void VirtualUartSendData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

unsigned int VirtualUartGetBytesNum(UART_TYPE eCom);

unsigned int VirtualUartGetData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

void VirtualUartBufClear(UART_TYPE eCom);

#endif

#ifdef __cplusplus
}
#endif

#endif /* __VIRTUAL_UART_LL_H__ */

/************************ (C) COPYRIGHT iESLab *****END OF FILE****/
