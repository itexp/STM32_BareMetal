/**
  ******************************************************************************
  * @file    UART_STM32F7_LL.h
  * @author  ZCShou
  * @version 1.1.0
  * @date    2019.6.18
  * @brief   STM32F7xx USART 驱动程序（基于 LL 库）
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 iESlab </center></h2>
  *
  *  (1) 在配置文件 ConfigDrivers.h 中进行配置
  *
  *  (2) 暂不支持轮询模式；中断模式暂未测试
  *
  *  (3)
  *
  ******************************************************************************
  */

#ifndef __UART_STM32F7_LL_H__
#define __UART_STM32F7_LL_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "UART.h"   /* 接口文件定义了对硬件的抽象 */
#if USART1_USED == 1 || USART2_USED == 1 || USART3_USED == 1 || UART4_USED == 1 || UART5_USED == 1
/* Exported defines ------------------------------------------------------------*/
#if USART1_USED == 1
    #ifndef USART1_RX_BUF_SIZE
    #define USART1_RX_BUF_SIZE                        512
    #endif

    #ifndef USART1_TX_BUF_SIZE
    #define USART1_TX_BUF_SIZE                        512
    #endif

    #if (USART1_RX_MODE != 0 && USART1_RX_MODE != 1 && USART1_RX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif

    #if (USART1_TX_MODE != 0 && USART1_TX_MODE != 1 && USART1_TX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif
#endif
#if USART2_USED == 1
    #ifndef USART2_RX_BUF_SIZE
    #define USART2_RX_BUF_SIZE                        512
    #endif

    #ifndef USART2_TX_BUF_SIZE
    #define USART2_TX_BUF_SIZE                        512
    #endif

    #if (USART2_RX_MODE != 0 && USART2_RX_MODE != 1 && USART2_RX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif

    #if (USART2_TX_MODE != 0 && USART2_TX_MODE != 1 && USART2_TX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif
#endif
#if USART3_USED == 1
    #ifndef USART3_RX_BUF_SIZE
    #define USART3_RX_BUF_SIZE                        512
    #endif

    #ifndef USART3_TX_BUF_SIZE
    #define USART3_TX_BUF_SIZE                        512
    #endif

    #if (USART3_RX_MODE != 0 && USART3_RX_MODE != 1 && USART3_RX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif

    #if (USART3_TX_MODE != 0 && USART3_TX_MODE != 1 && USART3_TX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif
#endif
#if UART4_USED == 1
    #ifndef UART4_RX_BUF_SIZE
    #define UART4_RX_BUF_SIZE                        512
    #endif

    #ifndef UART4_TX_BUF_SIZE
    #define UART4_TX_BUF_SIZE                        512
    #endif

    #if (UART4_RX_MODE != 0 && UART4_RX_MODE != 1 && UART4_RX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif

    #if (UART4_TX_MODE != 0 && UART4_TX_MODE != 1 && UART4_TX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif
#endif
#if UART5_USED == 1
    #ifndef UART5_RX_BUF_SIZE
    #define UART5_RX_BUF_SIZE                        512
    #endif

    #ifndef UART5_TX_BUF_SIZE
    #define UART5_TX_BUF_SIZE                        512
    #endif

    #if (UART5_RX_MODE != 0 && UART5_RX_MODE != 1 && UART5_RX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif

    #if (UART5_TX_MODE != 0 && UART5_TX_MODE != 1 && UART5_TX_MODE != 2)
    #error Only 0 or 1 or 2 can be used!
    #endif
#endif
/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
bool STM32F7UART_Init(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop );

void STM32F7UART_DeInit(UART_TYPE eCom);

void STM32F7UART_SendData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

unsigned int STM32F7UART_GetBytesNum(UART_TYPE eCom);

unsigned int STM32F7UART_GetData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

void STM32F7UART_ClearBuffer(UART_TYPE eCom);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __UART_STM32F7_LL_H__ */

/************************ (C) COPYRIGHT iESLab *****END OF FILE****/
