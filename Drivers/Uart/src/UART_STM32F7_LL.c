/**
  ******************************************************************************
  * @file    UART_STM32F7_LL.c
  * @author  ZCShou
  * @version 1.1.0
  * @date    2019.6.18
  * @brief   STM32F7xx USART 驱动程序（基于 LL 库）
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 iESlab </center></h2>
  *
  *  (1) 在配置文件 ConfigDrivers.h 中进行配置
  *
  *  (2) 暂不支持轮询模式；中断模式暂未测试
  *
  *  (3)
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "../inc/UART_STM32F7_LL.h"
#if USART1_USED == 1 || USART2_USED == 1 || USART3_USED == 1 || UART4_USED == 1 || UART5_USED == 1
#include "stm32f7xx_ll_usart.h"
#include "stm32f7xx_ll_gpio.h"
#include "stm32f7xx_ll_system.h"
#include "stm32f7xx_ll_dma.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_bus.h"
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
#if USART1_USED == 1
/**
 * @brief USART1结构
 */
typedef struct USART1_INFO
{
    unsigned char        Rx_Buf[USART1_RX_BUF_SIZE];
    volatile unsigned short int        Rx_RD_Index;
    volatile unsigned short int        Rx_WR_Index;
    unsigned char        Tx_Buf[USART1_TX_BUF_SIZE];
    volatile unsigned short int        Tx_RD_Index;
    volatile unsigned short int        Tx_WR_Index;
} USART1_INFO;
#endif

#if USART2_USED == 1
/**
 * @brief USART2结构
 */
typedef struct USART2_INFO
{
    unsigned char        Rx_Buf[USART2_RX_BUF_SIZE];
    volatile unsigned short int        Rx_RD_Index;
    volatile unsigned short int        Rx_WR_Index;
    unsigned char        Tx_Buf[USART2_TX_BUF_SIZE];
    volatile unsigned short int        Tx_RD_Index;
    volatile unsigned short int        Tx_WR_Index;
} USART2_INFO;
#endif


#if USART3_USED == 1
/**
 * @brief USART3结构
 */
typedef struct USART3_INFO
{
    unsigned char        Rx_Buf[USART3_RX_BUF_SIZE];
    volatile unsigned short int        Rx_RD_Index;
    volatile unsigned short int        Rx_WR_Index;
    unsigned char        Tx_Buf[USART3_TX_BUF_SIZE];
    volatile unsigned short int        Tx_RD_Index;
    volatile unsigned short int        Tx_WR_Index;
} USART3_INFO;
#endif

#if UART4_USED == 1
/**
 * @brief UART4结构
 */
typedef struct UART4_INFO
{
    unsigned char        Rx_Buf[UART4_RX_BUF_SIZE];
    volatile unsigned short int        Rx_RD_Index;
    volatile unsigned short int        Rx_WR_Index;
    unsigned char        Tx_Buf[UART4_TX_BUF_SIZE];
    volatile unsigned short int        Tx_RD_Index;
    volatile unsigned short int        Tx_WR_Index;
} UART4_INFO;
#endif

#if UART5_USED == 1
/**
 * @brief UART5结构
 */
typedef struct UART5_INFO
{
    unsigned char        Rx_Buf[UART5_RX_BUF_SIZE];
    volatile unsigned short int        Rx_RD_Index;
    volatile unsigned short int        Rx_WR_Index;
    unsigned char        Tx_Buf[UART5_TX_BUF_SIZE];
    volatile unsigned short int        Tx_RD_Index;
    volatile unsigned short int        Tx_WR_Index;
} UART5_INFO;
#endif
/* Private variables ---------------------------------------------------------*/
#if USART1_USED == 1
USART1_INFO Uart1Info = {0};
#endif
#if USART2_USED == 1
USART2_INFO Uart2Info = {0};
#endif
#if USART3_USED == 1
USART3_INFO Uart3Info = {0};
#endif
#if UART4_USED == 1
UART4_INFO Uart4Info = {0};
#endif
#if UART5_USED == 1
UART5_INFO Uart5Info = {0};
#endif
/* Private macro -------------------------------------------------------------*/
#ifndef USART_FEED_WEG
#define USART_FEED_WEG()
#endif
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup Uart Init Config functions
 *  @brief   对使用到的串口进行初始化配置
 *
@verbatim
 ===============================================================================
               ##### Uart Init Config functions #####
 ===============================================================================
    [..]
    [..]
    [..]
@endverbatim
  * @{
  */

/** 串口使用的GPIO配置
 *
 * @param[in] void
 * @retval NULL
**/
static void STM32F7UART_GPIO_Config(USART_TypeDef * eCom)
{
    LL_GPIO_InitTypeDef LL_GPIO_Struct = {0};

    /* 相关参数（共用） */
    LL_GPIO_StructInit(&LL_GPIO_Struct);
    LL_GPIO_Struct.Mode = LL_GPIO_MODE_ALTERNATE;
    LL_GPIO_Struct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Struct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    LL_GPIO_Struct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    LL_GPIO_Struct.Alternate = LL_GPIO_AF_7;

    #if USART1_USED == 1
    if (eCom == USART1)
    {
        /* 时钟 */
        USART1_GPIO_CLK_ENABLE();
        /* 引脚 */
        LL_GPIO_Struct.Pin = USART1_TX_PIN;
        LL_GPIO_Init(USART1_TX_GPIOx, &LL_GPIO_Struct);
        LL_GPIO_Struct.Pin = USART1_RX_PIN;
        LL_GPIO_Init(USART1_TX_GPIOx, &LL_GPIO_Struct);
    }
    #endif
}

/** 串口使用的DMA配置
 *
 * @param[in] eDir          传输方向
 * @retval NULL
**/
#if (USART1_RX_MODE == 0 || USART1_TX_MODE == 0)
static void STM32F7UART_DMA_Config(USART_TypeDef * eCom)
{
    LL_DMA_InitTypeDef DMA_InitStruct = {0};

    #if USART1_USED == 1
    if (eCom == USART1)
    {
        #if (USART1_TX_MODE == 0) || (USART1_RX_MODE == 0)
        /* 时钟 */
        USART1_DMA_CLK_ENABLE();    /* 使能DMA模块的时钟 */
        #endif

        /* 发送 DMA */
        #if (USART1_TX_MODE == 0)
        /* 先反初始化，防止多次调用初始化时出错 */
        LL_DMA_DeInit(USART1_DMA, USART1_DMA_TX_STREAM);
        NVIC_DisableIRQ(USART1_DMA_TX_IRQn);
        /* 相关参数 */
        DMA_InitStruct.Mode = LL_DMA_MODE_NORMAL;
        DMA_InitStruct.Direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH;
        DMA_InitStruct.Priority = LL_DMA_PRIORITY_HIGH;
        DMA_InitStruct.PeriphOrM2MSrcAddress = (uint32_t)(&(USART1->TDR));
        DMA_InitStruct.PeriphOrM2MSrcIncMode = LL_DMA_PERIPH_NOINCREMENT;
        DMA_InitStruct.PeriphOrM2MSrcDataSize = LL_DMA_PDATAALIGN_BYTE;
        DMA_InitStruct.MemoryOrM2MDstAddress = (uint32_t)Uart1Info.Tx_Buf;
        DMA_InitStruct.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT;
        DMA_InitStruct.MemoryOrM2MDstDataSize = LL_DMA_MDATAALIGN_BYTE;
        DMA_InitStruct.NbData = 0;
        DMA_InitStruct.Channel = USART1_DMA_TX_CHANNEL;
        LL_DMA_Init(USART1_DMA, USART1_DMA_TX_STREAM, &DMA_InitStruct);
        /* 中断配置 */
        LL_DMA_EnableIT_TC(USART1_DMA, USART1_DMA_TX_STREAM);
        NVIC_SetPriority(USART1_DMA_TX_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
        NVIC_EnableIRQ(USART1_DMA_TX_IRQn);
        #endif

        /* 接收 DMA */
        #if (USART1_RX_MODE == 0)
        /* 先反初始化，防止多次调用初始化时出错 */
        LL_DMA_DeInit(USART1_DMA, USART1_DMA_RX_STREAM);
        /* 相关参数 */
        DMA_InitStruct.Mode = LL_DMA_MODE_CIRCULAR;
        DMA_InitStruct.Direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY;
        DMA_InitStruct.Priority = LL_DMA_PRIORITY_HIGH;
        DMA_InitStruct.PeriphOrM2MSrcAddress = (uint32_t)(&(USART1->RDR));
        DMA_InitStruct.PeriphOrM2MSrcIncMode = LL_DMA_PERIPH_NOINCREMENT;
        DMA_InitStruct.PeriphOrM2MSrcDataSize = LL_DMA_PDATAALIGN_BYTE;
        DMA_InitStruct.MemoryOrM2MDstAddress = (uint32_t)Uart1Info.Rx_Buf;
        DMA_InitStruct.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT;
        DMA_InitStruct.MemoryOrM2MDstDataSize = LL_DMA_MDATAALIGN_BYTE;
        DMA_InitStruct.NbData = USART1_RX_BUF_SIZE;
        DMA_InitStruct.Channel = USART1_DMA_RX_CHANNEL;
        LL_DMA_Init(USART1_DMA, USART1_DMA_RX_STREAM, &DMA_InitStruct);
        #endif
    }
    #endif
}
#endif

/** 串口配置
 *
 * @param[in] baud      波特率
 * @param[in] dataBit   数据位
 * @param[in] parity    校验位
 * @param[in] stop      停止位
 * @retval NULL
**/
static void STM32F7UART_Config(USART_TypeDef * eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop)
{
    LL_USART_InitTypeDef UART_InitStruct = {0};

    UART_InitStruct.BaudRate = baud;
    UART_InitStruct.DataWidth = LL_USART_DATAWIDTH_9B;        /* 数据位数 默认均为9b */

    if (parity == UART_PARITY_BIT_NULL)
    {
        UART_InitStruct.Parity = LL_USART_PARITY_NONE;
        UART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;            /* 没有校验位时 数据长度为8 */
    }
    else if ( parity == UART_PARITY_BIT_ODD )
    {
        UART_InitStruct.Parity = LL_USART_PARITY_ODD;
    }
    else
    {
        UART_InitStruct.Parity = LL_USART_PARITY_EVEN;
    }

    if (stop == UART_STOP_BIT_2)
    {
        UART_InitStruct.StopBits = LL_USART_STOPBITS_2;
    }
    else
    {
        UART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    }

    if (dataBit == UART_DATA_BIT_8)
    {
        UART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    }
    else
    {
        UART_InitStruct.DataWidth = LL_USART_DATAWIDTH_9B;
    }

    UART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;

    UART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;

    #if USART1_USED == 1
    if (eCom == USART1)
    {
        /* 先反初始化，防止多次调用初始化时出错 */
        LL_USART_DeInit(USART1);
        /* 时钟 */
        USART1_CLK_ENABLE();
        /* 参数设置 */
        LL_USART_Init(USART1, &UART_InitStruct);
        /* 中断 */
        #if (USART1_RX_MODE == 0)
            LL_USART_EnableIT_IDLE(USART1);
        #elif (USART1_RX_MODE == 1)
            LL_USART_EnableIT_RXNE(USART1);
        #else
        #endif
        NVIC_SetPriority(USART1_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
        NVIC_EnableIRQ(USART1_IRQ);

        #if (USART1_RX_MODE == 0)
            LL_USART_EnableDMAReq_RX(USART1);    /* 启动串口的DMA接收*/
        #endif

        LL_USART_Enable(USART1);
    }
    #endif
}

#if USART1_USED == 1
/** 串口中断处理函数
 *
 * @param[in]    void
 * @retval        NULL
**/
void USART1_IRQHandler(void)
{
    /* USART in mode Tramitter -------------------------------------------------*/
    #if USART1_TX_MODE == 0
    if (LL_USART_IsActiveFlag_TC(USART1) == 1)
    {
        LL_USART_ClearFlag_TC(USART1);
        LL_USART_DisableIT_TC(USART1);
        Uart1Info.Tx_RD_Index = Uart1Info.Tx_WR_Index;
    }
    #elif USART1_TX_MODE == 1
    if (LL_USART_IsActiveFlag_TXE(USART1) == 1)
    {
        /* TXE pending bit is cleared only by a write to the USART_DR register (USART_SendData()).*/
        if (Uart1Info.Tx_RD_Index != Uart1Info.Tx_WR_Index)
        {
            LL_USART_TransmitData8(USART1, Uart1Info.Tx_Buf[Uart1Info.Tx_RD_Index++]);
            if (Uart1Info.Tx_RD_Index >= USART1_TX_BUF_SIZE)
            {
                Uart1Info.Tx_RD_Index = 0;
            }
        }
        else
        {
            LL_USART_DisableIT_TXE(USART1);
        }
    }
    #endif

    /* USART in mode Receiver --------------------------------------------------*/
    #if (USART1_RX_MODE == 0)        /* DMA 模式时，使用空闲中断 */
    if (LL_USART_IsActiveFlag_IDLE(USART1) == 1)
    {
        LL_USART_ClearFlag_IDLE(USART1);
        Uart1Info.Rx_WR_Index = USART1_RX_BUF_SIZE - LL_DMA_GetDataLength(USART1_DMA, USART1_DMA_RX_STREAM);
    }
    #elif USART1_RX_MODE == 1
    if (LL_USART_IsActiveFlag_RXNE(USART1) == 1)
    {
        Uart1Info.Rx_Buf[Uart1Info.Rx_WR_Index++] = LL_USART_ReceiveData8(USART1);
        if (Uart1Info.Rx_WR_Index >= USART1_RX_BUF_SIZE)
        {
            Uart1Info.Rx_WR_Index = 0;
        }
    }
    #endif
    /* USART Error interrupt--------------------------------------- */
    if (LL_USART_IsActiveFlag_LBD(USART1) == 1)
    {
        LL_USART_ClearFlag_LBD(USART1);
    }
    if (LL_USART_IsActiveFlag_ORE(USART1))
    {
        LL_USART_ClearFlag_ORE(USART1);
    }
    if (LL_USART_IsActiveFlag_FE(USART1))
    {
        LL_USART_ClearFlag_FE(USART1);
    }
}
#endif

#if USART1_USED == 1 && USART1_TX_MODE == 0
/** 串口1使用的DMA中断处理函数
 *
 * @param[in]    void
 * @retval        NULL
**/
void DMA2_Stream7_IRQHandler(void)
{
    if (LL_DMA_IsActiveFlag_TC7(USART1_DMA) == 1) /* 发送完成中断标志 */
    {
        LL_DMA_ClearFlag_TC7(USART1_DMA);
        LL_USART_EnableIT_TC(USART1);        /* DMA发送完成时，串口不一定完成，所以这里开启串口完成中断 */
        LL_USART_ClearFlag_TC(USART1);
    }
    /* Error interrupt--------------------------------------- */
    if (LL_DMA_IsActiveFlag_HT7(USART1_DMA) == 1)
    {
        LL_DMA_ClearFlag_HT7(USART1_DMA);
    }
    if (LL_DMA_IsActiveFlag_TE7(USART1_DMA) == 1)
    {
        LL_DMA_ClearFlag_TE7(USART1_DMA);
    }
    if (LL_DMA_IsActiveFlag_DME7(USART1_DMA) == 1)
    {
        LL_DMA_ClearFlag_DME7(USART1_DMA);
    }
    if (LL_DMA_IsActiveFlag_FE7(USART1_DMA) == 1)
    {
        LL_DMA_ClearFlag_FE7(USART1_DMA);
    }
    /* 发送完成时，关闭通道 */
    LL_USART_DisableDMAReq_TX(USART1);
}
#endif

/** 通信口初始化
 *
 * @param[in] eCom      指定的通信口
 * @param[in] baud      波特率
 * @param[in] dataBit   数据位
 * @param[in] parity    校验位
 * @param[in] stop      停止位
 * @retval 是否成功
**/
bool STM32F7UART_Init(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop)
{
    /* 串口引脚配置 */
    STM32F7UART_GPIO_Config((USART_TypeDef *)eCom);

    /* Buf */
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        Uart1Info.Rx_RD_Index = 0;
        Uart1Info.Rx_WR_Index = 0;
        Uart1Info.Tx_RD_Index = 0;
        Uart1Info.Tx_WR_Index = 0;
        #if USART1_RX_MODE == 0
        STM32F7UART_DMA_Config(USART1);
        #endif
    }
    #endif

    /* 串口配置 */
    STM32F7UART_Config((USART_TypeDef *)eCom, baud, dataBit, parity, stop);

    return true;
}

/** 通信口失能
 *
 * @param[in] eCom      指定的通信口
 * @retval NULL
**/
void STM32F7UART_DeInit(UART_TYPE eCom)
{
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        LL_USART_DeInit(USART1);
        NVIC_DisableIRQ(USART1_IRQ);
        #if (USART1_TX_MODE == 0)
        LL_DMA_DeInit(USART1_DMA, USART1_DMA_TX_STREAM);
        NVIC_DisableIRQ(USART1_DMA_TX_IRQn);
        #endif
        #if (USART1_RX_MODE == 0)
        LL_DMA_DeInit(USART1_DMA, USART1_DMA_RX_STREAM);
        #endif
        Uart1Info.Rx_RD_Index = 0;
        Uart1Info.Rx_WR_Index = 0;
        Uart1Info.Tx_RD_Index = 0;
        Uart1Info.Tx_WR_Index = 0;
    }
    #endif
}

/**
  * @}
  */

/** @defgroup Uart read and write
 *  @brief   串口读写
 *
@verbatim
 ===============================================================================
               ##### Uart read and write #####
 ===============================================================================
    [..]
    [..]
         (#)
@endverbatim
  * @{
  */

/** 串口发送数据
 *
 * @param[in] eCom      指定的通信口
 * @param[in] pData     要发送的数据
 * @param[in] len       要发送的数据长度
 * @retval void
**/
void STM32F7UART_SendData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    if (pData == NULL || len == 0)
    {
        return;
    }
    #if USART1_USED == 1
        if ((USART_TypeDef *)eCom == USART1)
        {
            #if (USART1_TX_MODE == 0)
                memcpy(Uart1Info.Tx_Buf, pData, len);
                Uart1Info.Tx_WR_Index = len;
                Uart1Info.Tx_RD_Index = 0;
                LL_DMA_DisableStream(USART1_DMA, USART1_DMA_TX_STREAM); /* 只有关闭时，才可以设置 发送数量 */
                LL_DMA_SetDataLength(USART1_DMA, USART1_DMA_TX_STREAM, len);
                LL_DMA_EnableStream(USART1_DMA, USART1_DMA_TX_STREAM);
                LL_USART_EnableDMAReq_TX(USART1);
            #elif (USART1_TX_MODE == 1)
                while ((USART1_TX_BUF_SIZE-(USART1_TX_BUF_SIZE + Uart1Info.Tx_WR_Index - Uart1Info.Tx_RD_Index)%USART1_TX_BUF_SIZE) < len)  /* 直到空间足够 */
                {
                    USART_FEED_WEG();
                }
                for (int i = 0; i < len; ++i)
                {
                    Uart1Info.Tx_Buf[Uart1Info.Tx_WR_Index++] = pData[i];
                    if (Uart1Info.Tx_WR_Index >= USART1_TX_BUF_SIZE)
                    {
                        Uart1Info.Tx_WR_Index = 0;
                    }
                }
                LL_USART_EnableIT_TXE(USART1);/* 启动中断发送 */
            #else
            #endif
            while (Uart1Info.Tx_WR_Index != Uart1Info.Tx_RD_Index)      /* 等待之前的发送完成 */
            {
                USART_FEED_WEG();
            }
        }
    #endif
}

/** 串口获取未读数据个数（单位字节）
 *
 * @param[in] eCom      指定的通信口
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int STM32F7UART_GetBytesNum(UART_TYPE eCom)
{
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        return (USART1_RX_BUF_SIZE + Uart1Info.Rx_WR_Index - Uart1Info.Rx_RD_Index)%USART1_RX_BUF_SIZE;
    }
    #endif
    return 0;
}

/** 串口获取数据
 *
 * @param[in] eCom      指定的通信口
 * @param[out] pData    存放数据
 * @param[in] len       要获取的数据长度
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int STM32F7UART_GetData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    int i = 0;

    if (len == 0 || pData == NULL || len > STM32F7UART_GetBytesNum(eCom))
    {
        return 0;
    }
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        if (Uart1Info.Rx_RD_Index == Uart1Info.Rx_WR_Index)
        {
            return 0;
        }
        else
        {
            for (i = 0; i < len; i++)
            {
                pData[i] = Uart1Info.Rx_Buf[Uart1Info.Rx_RD_Index++];
                if (Uart1Info.Rx_RD_Index == USART1_RX_BUF_SIZE)        /* 到达缓冲区末尾 */
                {
                    Uart1Info.Rx_RD_Index = 0;
                }
            }
        }
    }
    #endif

    return i;
}

/** 清空收发缓冲区
 *
 * @param[in] eCom        通信口
 * @retval void
**/
void STM32F7UART_ClearBuffer(UART_TYPE eCom)
{
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        memset(Uart1Info.Rx_Buf, 0x00, USART1_RX_BUF_SIZE);
        Uart1Info.Rx_RD_Index = Uart1Info.Rx_WR_Index;            /* 这里必须赋值为 读=写，因为有DMA问题 */
        Uart1Info.Tx_RD_Index = Uart1Info.Tx_WR_Index;
    }
    #endif
}

/** 填充接收缓冲区（用于测试）
 *
 * @param[in] eCom          通信口
 * @param[in] pData         数据
 * @param[in] len           数据长度
 * @retval void
**/
void STM32F7UART_FillRcvBuf(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    #if USART1_USED == 1
    if ((USART_TypeDef *)eCom == USART1)
    {
        memcpy(Uart1Info.Rx_Buf, pData, len);
        Uart1Info.Rx_RD_Index = 0;
        Uart1Info.Rx_WR_Index = len;
    }
    #endif
}

/**
  * @}
  */
#endif

/************************ (C) COPYRIGHT iESLab *****END OF FILE****/
