/**
  ******************************************************************************
  * @file    VirtualUart_LL.c
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   模拟串口驱动实现源文件（基于 LL 库）
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "VirtualUart_LL.h"
#if VIRTUAL_UART1 == 1 || VIRTUAL_UART2 == 2 || VIRTUAL_UART3 == 3
#include "STM32F7xx_ll_bus.h"
#include "STM32F7xx_ll_gpio.h"
#include "STM32F7xx_ll_exti.h"
#include "STM32F7xx_ll_tim.h"
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/** 
 * @brief 接收步骤
 */
typedef enum VCOM_STEP
{
    VCOM_STEP_START = 0,
    VCOM_STEP_DATA,
    VCOM_STEP_PARITY,
    VCOM_STEP_STOP,
    VCOM_STEP_END
} VCOM_STEP;

/** 
  * @brief 传输方向枚举
  */
typedef enum VCOM_DIR_TYPE
{
    VCOM_DIR_TX = 1<<0,
    VCOM_DIR_RX = 1<<1
} VCOM_DIR_TYPE;

/** 
 * @brief 配置
 */
typedef struct VIRTUAL_UART_CONFIG
{
    unsigned char       data;
    unsigned char       parity;
    unsigned char       stop;
} VIRTUAL_UART_CONFIG;

/** 
 * @brief 步骤
 */
typedef struct VIRTUAL_UART_STEP
{
    unsigned char       step;
    unsigned char       freq;
    unsigned char       counter;
    unsigned char       bit1Num;
    unsigned char       bitParity;
} VIRTUAL_UART_STEP;

#if VIRTUAL_UART1 == 1
/** 
 * @brief VIRTUAL_UART1结构
 */
typedef struct VIRTUAL_UART1_INFO
{
    VIRTUAL_UART_CONFIG Config;
    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3
    VIRTUAL_UART_STEP   StepRcv;
    #endif
    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2
    VIRTUAL_UART_STEP   StepSnd;
    #endif
    #if VIRTUAL_UART1_MODE == 1
    VCOM_DIR_TYPE       dir;
    unsigned int        RcvPeriod;
    unsigned int        SndPeriod;
    #endif
    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3
    unsigned char       Rx_Buf[VIRTUAL_UART1_RX_BUF_SIZE];
    volatile unsigned short int         Rx_RD_Index;
    volatile unsigned short int         Rx_WR_Index;
    #endif
    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2
    unsigned char       Tx_Buf[VIRTUAL_UART1_TX_BUF_SIZE];
    volatile unsigned short int         Tx_RD_Index;
    volatile unsigned short int         Tx_WR_Index;
    #endif
} VIRTUAL_UART1_INFO;
#endif

#if VIRTUAL_UART2 == 2
/** 
 * @brief VIRTUAL_UART2结构
 */
typedef struct VIRTUAL_UART2_INFO
{
    VIRTUAL_UART_CONFIG Config;
    #if VIRTUAL_UART2_MODE == 0 || VIRTUAL_UART2_MODE == 1 || VIRTUAL_UART2_MODE == 3
    VIRTUAL_UART_STEP   StepRcv;
    #endif
    #if VIRTUAL_UART2_MODE == 0 || VIRTUAL_UART2_MODE == 1 || VIRTUAL_UART2_MODE == 2
    VIRTUAL_UART_STEP   StepSnd;
    #endif
    #if VIRTUAL_UART2_MODE == 1
    VCOM_DIR_TYPE       dir;
    unsigned int        RcvPeriod;
    unsigned int        SndPeriod;
    #endif
    #if VIRTUAL_UART2_MODE == 0 || VIRTUAL_UART2_MODE == 1 || VIRTUAL_UART2_MODE == 3
    unsigned char       Rx_Buf[VIRTUAL_UART2_RX_BUF_SIZE];
    volatile unsigned short int         Rx_RD_Index;
    volatile unsigned short int         Rx_WR_Index;
    #endif
    #if VIRTUAL_UART2_MODE == 0 || VIRTUAL_UART2_MODE == 1 || VIRTUAL_UART2_MODE == 2
    unsigned char       Tx_Buf[VIRTUAL_UART2_TX_BUF_SIZE];
    volatile unsigned short int         Tx_RD_Index;
    volatile unsigned short int         Tx_WR_Index;
    #endif
} VIRTUAL_UART2_INFO;
#endif

#if VIRTUAL_UART3 == 3
/** 
 * @brief VIRTUAL_UART3结构
 */
typedef struct VIRTUAL_UART3_INFO
{
    VIRTUAL_UART_CONFIG Config;
    #if VIRTUAL_UART3_MODE == 0 || VIRTUAL_UART3_MODE == 1 || VIRTUAL_UART3_MODE == 3
    VIRTUAL_UART_STEP   StepRcv;
    #endif
    #if VIRTUAL_UART3_MODE == 0 || VIRTUAL_UART3_MODE == 1 || VIRTUAL_UART3_MODE == 2
    VIRTUAL_UART_STEP   StepSnd;
    #endif
    #if VIRTUAL_UART3_MODE == 1
    VCOM_DIR_TYPE       dir;
    unsigned int        RcvPeriod;
    unsigned int        SndPeriod;
    #endif
    #if VIRTUAL_UART3_MODE == 0 || VIRTUAL_UART3_MODE == 1 || VIRTUAL_UART3_MODE == 3
    unsigned char       Rx_Buf[VIRTUAL_UART3_RX_BUF_SIZE];
    volatile unsigned short int         Rx_RD_Index;
    volatile unsigned short int         Rx_WR_Index;
    #endif
    #if VIRTUAL_UART3_MODE == 0 || VIRTUAL_UART3_MODE == 1 || VIRTUAL_UART3_MODE == 2
    unsigned char       Tx_Buf[VIRTUAL_UART3_TX_BUF_SIZE];
    volatile unsigned short int         Tx_RD_Index;
    volatile unsigned short int         Tx_WR_Index;
    #endif
} VIRTUAL_UART3_INFO;
#endif
/* Private variables ---------------------------------------------------------*/
#if VIRTUAL_UART1 == 1
VIRTUAL_UART1_INFO VUart1Info = {0};
#endif
/* Private macro -------------------------------------------------------------*/
#ifndef VIRTUAL_UART_FEED_WEG
#define VIRTUAL_UART_FEED_WEG()
#endif
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup Uart Init Config functions
 *  @brief   对使用到的串口进行初始化配置 
 *  
@verbatim   
 ===============================================================================
               ##### Uart Init Config functions #####
 ===============================================================================
    [..] 
    [..] 
    [..] 
@endverbatim
  * @{
  */

/** 模拟串口的两个引脚
 *
 * @param[in] void
 * @retval NULL
**/
static void VirtualUartGPIOConfig(UART_TYPE eCom)
{
    LL_GPIO_InitTypeDef LL_GPIO_Struct = {0};
    LL_EXTI_InitTypeDef EXTI_InitStruct = {0};

    /* 相关参数（共用） */
    LL_GPIO_StructInit(&LL_GPIO_Struct);
    LL_GPIO_Struct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Struct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
    LL_GPIO_Struct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;

    LL_EXTI_StructInit(&EXTI_InitStruct);
    EXTI_InitStruct.LineCommand = ENABLE;
    EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
    EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
    
    /* 外部中断需要配置 AFIO */
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);

    #if VIRTUAL_UART1 == 1
    if (eCom == VIRTUAL_UART1)
    {
        /* 时钟 */
        VIRTUAL_UART1_GPIO_CLK_ENABLE();
        /* 相关参数 */
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2
        LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
        LL_GPIO_Struct.Mode = LL_GPIO_MODE_OUTPUT;
        LL_GPIO_Struct.Pin = VIRTUAL_UART1_TX_PIN;
        LL_GPIO_Init(VIRTUAL_UART1_TX_GPIOx, &LL_GPIO_Struct);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3
        LL_GPIO_Struct.Mode = LL_GPIO_MODE_INPUT;
        LL_GPIO_Struct.Pin = VIRTUAL_UART1_RX_PIN;
        LL_GPIO_Init(VIRTUAL_UART1_RX_GPIOx, &LL_GPIO_Struct);

        VIRTUAL_UART1_EXTI_Config();
        /* Configure EXTI */
        EXTI_InitStruct.Line_0_31 = VIRTUAL_UART1_EXTI_Line;
        LL_EXTI_Init(&EXTI_InitStruct);
        #endif
    }
    #endif
}

/** 定时器
 *
 * @param[in] void
 * @retval NULL
**/
static void VirtualUartTimerConfig(UART_TYPE eCom, UART_BAUDRATE_TYPE baud)
{
    LL_TIM_InitTypeDef TIM_InitStruct;
    LL_RCC_ClocksTypeDef RCC_Clocks;
    unsigned int Freq;

    /* 读取总线时钟，用来计算重装值 */
    LL_RCC_GetSystemClocksFreq(&RCC_Clocks);
    /* 参数 */
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;

    #if VIRTUAL_UART1 == 1
    if (eCom == VIRTUAL_UART1)
    {
        #if VIRTUAL_UART1_MODE == 1
        #if VIRTUAL_UART1_TX_RX_TIMER_APB == 1
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK1_Frequency > 1 ? RCC_Clocks.PCLK1_Frequency * 2 : RCC_Clocks.PCLK1_Frequency;
        #else
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK2_Frequency > 1 ? RCC_Clocks.PCLK2_Frequency * 2 : RCC_Clocks.PCLK2_Frequency;
        #endif
        /* 接收的定时器的时钟 */
        LL_TIM_DeInit(VIRTUAL_UART1_TX_RX_TIMER);
        VIRTUAL_UART1_TX_RX_TIMER_CLK_ENABLE();
        /* 接收的定时器的参数 */
        VUart1Info.RcvPeriod = Freq / (baud*2) / (0+1) - 1;
        VUart1Info.SndPeriod = Freq / baud / (0+1) - 1;
        TIM_InitStruct.Prescaler = VIRTUAL_UART1_TX_RX_TIMER_Prescaler;
        TIM_InitStruct.Autoreload = VUart1Info.RcvPeriod;
        LL_TIM_Init(VIRTUAL_UART1_TX_RX_TIMER, &TIM_InitStruct);
        /* 接收的定时器的中断 */
        LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER);/* 初始化后，更新中断标志就会触发，这里清除，否则多进入一次中断 */
        LL_TIM_EnableIT_UPDATE(VIRTUAL_UART1_TX_RX_TIMER);
        NVIC_SetPriority(VIRTUAL_UART1_TX_RX_TIMER_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
        NVIC_EnableIRQ(VIRTUAL_UART1_TX_RX_TIMER_IRQ);
        #endif

        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 2
        #if VIRTUAL_UART1_TX_TIMER_APB == 1
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK1_Frequency > 1 ? RCC_Clocks.PCLK1_Frequency * 2 : RCC_Clocks.PCLK1_Frequency;
        #else
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK2_Frequency > 1 ? RCC_Clocks.PCLK2_Frequency * 2 : RCC_Clocks.PCLK2_Frequency;
        #endif
        /* 发送的定时器的时钟 */
        LL_TIM_DeInit(VIRTUAL_UART1_TX_TIMER);
        VIRTUAL_UART1_TX_TIMER_CLK_ENABLE();
        /* 发送的定时器的参数 */
        TIM_InitStruct.Prescaler = VIRTUAL_UART1_TX_TIMER_Prescaler;
        TIM_InitStruct.Autoreload = Freq / baud / (0+1) - 1;
        LL_TIM_Init(VIRTUAL_UART1_TX_TIMER, &TIM_InitStruct);
        /* 发送的定时器的中断 */
        LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_TIMER);/* 初始化后，更新中断标志就会触发，这里清除，否则多进入一次中断 */
        LL_TIM_EnableIT_UPDATE(VIRTUAL_UART1_TX_TIMER);
        NVIC_SetPriority(VIRTUAL_UART1_TX_TIMER_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
        NVIC_EnableIRQ(VIRTUAL_UART1_TX_TIMER_IRQ);
        #endif

        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3
        #if VIRTUAL_UART1_RX_TIMER_APB == 1
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK1_Frequency > 1 ? RCC_Clocks.PCLK1_Frequency * 2 : RCC_Clocks.PCLK1_Frequency;
        #else
        Freq = RCC_Clocks.HCLK_Frequency / RCC_Clocks.PCLK2_Frequency > 1 ? RCC_Clocks.PCLK2_Frequency * 2 : RCC_Clocks.PCLK2_Frequency;
        #endif
        /* 接收的定时器的时钟 */
        LL_TIM_DeInit(VIRTUAL_UART1_RX_TIMER);
        VIRTUAL_UART1_RX_TIMER_CLK_ENABLE();
        /* 接收的定时器的参数 */
        TIM_InitStruct.Prescaler = VIRTUAL_UART1_RX_TIMER_Prescaler;
        TIM_InitStruct.Autoreload = Freq / (baud*2) / (0+1) - 1;
        LL_TIM_Init(VIRTUAL_UART1_RX_TIMER, &TIM_InitStruct);
        /* 接收的定时器的中断 */
        LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_RX_TIMER);/* 初始化后，更新中断标志就会触发，这里清除，否则多进入一次中断 */
        LL_TIM_EnableIT_UPDATE(VIRTUAL_UART1_RX_TIMER);
        NVIC_SetPriority(VIRTUAL_UART1_RX_TIMER_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
        NVIC_EnableIRQ(VIRTUAL_UART1_RX_TIMER_IRQ);
        #endif
    }
    #endif
}

/** 接收使能（进入中断状态）
 *
 * @param[in] void
 * @retval NULL
**/
static void VirtualUartRxStart(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3)
    if (eCom == VIRTUAL_UART1)
    {
        #if VIRTUAL_UART1_MODE == 1
        VUart1Info.dir = VCOM_DIR_RX;
        #endif
        #if VIRTUAL_UART1_MODE == 1
        LL_TIM_DisableCounter(VIRTUAL_UART1_TX_RX_TIMER);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3
        LL_TIM_DisableCounter(VIRTUAL_UART1_RX_TIMER);
        #endif
        NVIC_EnableIRQ(VIRTUAL_UART1_EXTI_IRQ);
        LL_EXTI_ClearFlag_0_31(VIRTUAL_UART1_EXTI_Line);
    }
    #endif
}

/** 继续接收使能（进入定时器状态）
 *
 * @param[in] void
 * @retval NULL
**/
static void VirtualUartRxContinue(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3)
    if (eCom == VIRTUAL_UART1)
    {
        #if VIRTUAL_UART1_MODE == 1
        LL_TIM_EnableCounter(VIRTUAL_UART1_TX_RX_TIMER);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3
        LL_TIM_EnableCounter(VIRTUAL_UART1_RX_TIMER);
        #endif
        NVIC_DisableIRQ(VIRTUAL_UART1_EXTI_IRQ);
        LL_EXTI_ClearFlag_0_31(VIRTUAL_UART1_EXTI_Line);
        #if VIRTUAL_UART1_MODE == 1
        VUart1Info.dir = VCOM_DIR_RX;
        #endif
    }
    #endif
}

/** 外部中断处理函数
 *
 * @param[in] void
 * @retval NULL
**/
void VirtualUartExtiIrqHandler(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3)
    if (eCom == VIRTUAL_UART1)
    {
        LL_EXTI_ClearFlag_0_31(VIRTUAL_UART1_EXTI_Line);
        #if VIRTUAL_UART1_MODE == 1
        if (VUart1Info.dir == VCOM_DIR_TX)
        {
            return;         /* 半双工下，如果在发送，不允许接收 */
        }
        #endif
        if (LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN) == 0)
        {
            VirtualUartRxContinue((UART_TYPE)VIRTUAL_UART1);
            VUart1Info.StepRcv.step = VCOM_STEP_START;
            VUart1Info.StepRcv.freq = 0;
        }
    }
    #endif
}

#if VIRTUAL_UART1 == 1
/** 针对虚拟串口1的定时器中断处理函数
 *
 * @param[in] dir       接收还是发送
 * @retval void
**/
void VirtualUart1RXTimerIrqHandler(void)
{
    unsigned char byte;

    #if VIRTUAL_UART1_MODE == 1
    if (VUart1Info.dir == VCOM_DIR_RX)
    {
        if (LL_TIM_IsActiveFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER) != 0)
        {
            LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER);
            if (VUart1Info.StepRcv.freq % 2 == 0)
            {
                switch (VUart1Info.StepRcv.step)
                {
                    case VCOM_STEP_START:
                    {
                        VUart1Info.StepRcv.step = VCOM_STEP_DATA;
                        VUart1Info.StepRcv.counter = 0;
                        VUart1Info.StepRcv.bit1Num = 0;
                        VUart1Info.StepRcv.freq++;
                        VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;
                        break;
                    }
                    case VCOM_STEP_DATA:
                    {
                        byte = LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN);
                        if (byte == 1)
                        {
                            VUart1Info.StepRcv.bit1Num++;
                            VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] |= (byte << VUart1Info.StepRcv.counter);
                        }
                        
                        VUart1Info.StepRcv.counter++;
                        if (VUart1Info.StepRcv.counter >= VUart1Info.Config.data)
                        {
                            if (VUart1Info.Config.parity != UART_PARITY_BIT_NULL)
                            {
                                VUart1Info.StepRcv.step = VCOM_STEP_PARITY;
                            }
                            else
                            {
                                VUart1Info.StepRcv.step = VCOM_STEP_STOP;
                            }
                            VUart1Info.StepRcv.counter = 0;
                        }
                        VUart1Info.StepRcv.freq++;
                        break;
                    }
                    case VCOM_STEP_PARITY:
                    {
                        if (LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN) == 1)
                        {
                            VUart1Info.StepRcv.bit1Num++;
                        }
                        
                        /* 检查校验位是否正确 */
                        if ((VUart1Info.Config.parity == UART_PARITY_BIT_ODD && VUart1Info.StepRcv.bit1Num%2 != 0)
                        || (VUart1Info.Config.parity == UART_PARITY_BIT_EVEN && VUart1Info.StepRcv.bit1Num%2 == 0))
                        {
                            VUart1Info.StepRcv.step = VCOM_STEP_STOP;
                            VUart1Info.StepRcv.freq++;
                        }
                        else
                        {
                            /* 校验出错，清除之前收到的bit */
                            VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;
                            VUart1Info.StepRcv.step = VCOM_STEP_START;
                            VUart1Info.StepRcv.freq = 0;
                            VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                        }
                        break;
                    }
                    case VCOM_STEP_STOP:
                    {
                        if (LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN) == 1)
                        {
                            VUart1Info.StepRcv.counter++;
                            if (VUart1Info.StepRcv.counter >= VUart1Info.Config.stop)     /* 注意：这里实际丢了半个停止位 */
                            {
                                VUart1Info.StepRcv.counter = 0;
                                VUart1Info.Rx_WR_Index++;
                                if (VUart1Info.Rx_WR_Index >= VIRTUAL_UART1_RX_BUF_SIZE)
                                {
                                    VUart1Info.Rx_WR_Index = 0;
                                }
                                VUart1Info.StepRcv.step = VCOM_STEP_START;
                                VUart1Info.StepRcv.freq = 0;
                                VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                            }
                            else
                            {
                                VUart1Info.StepRcv.freq++;
                            }
                        }
                        else
                        {
                            VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;   /* 停止位错误，清除之前收到的bit */
                            VUart1Info.StepRcv.step = VCOM_STEP_START;
                            VUart1Info.StepRcv.freq = 0;
                            VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                        }
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
            else
            {
                VUart1Info.StepRcv.freq++;
            }
        }
    }
    #endif

    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3
    if (LL_TIM_IsActiveFlag_UPDATE(VIRTUAL_UART1_RX_TIMER) != 0)
    {
        LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_RX_TIMER);
        if (VUart1Info.StepRcv.freq % 2 == 0)
        {
            switch (VUart1Info.StepRcv.step)
            {
                case VCOM_STEP_START:
                {
                    VUart1Info.StepRcv.step = VCOM_STEP_DATA;
                    VUart1Info.StepRcv.counter = 0;
                    VUart1Info.StepRcv.bit1Num = 0;
                    VUart1Info.StepRcv.freq++;
                    VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;
                    break;
                }
                case VCOM_STEP_DATA:
                {
                    byte = LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN);
                    if (byte == 1)
                    {
                        VUart1Info.StepRcv.bit1Num++;
                        VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] |= (byte << VUart1Info.StepRcv.counter);
                    }
                    
                    VUart1Info.StepRcv.counter++;
                    if (VUart1Info.StepRcv.counter >= VUart1Info.Config.data)
                    {
                        if (VUart1Info.Config.parity != UART_PARITY_BIT_NULL)
                        {
                            VUart1Info.StepRcv.step = VCOM_STEP_PARITY;
                        }
                        else
                        {
                            VUart1Info.StepRcv.step = VCOM_STEP_STOP;
                        }
                        VUart1Info.StepRcv.counter = 0;
                    }
                    VUart1Info.StepRcv.freq++;
                    break;
                }
                case VCOM_STEP_PARITY:
                {
                    if (LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN) == 0)
                    {
                        VUart1Info.StepRcv.bit1Num++;
                    }
                    
                    /* 检查校验位是否正确 */
                    if ((VUart1Info.Config.parity == UART_PARITY_BIT_ODD && VUart1Info.StepRcv.bit1Num%2 != 0)
                    || (VUart1Info.Config.parity == UART_PARITY_BIT_EVEN && VUart1Info.StepRcv.bit1Num%2 == 0))
                    {
                        VUart1Info.StepRcv.step = VCOM_STEP_STOP;
                        VUart1Info.StepRcv.freq++;
                    }
                    else
                    {
                        /* 校验出错，清除之前收到的bit */
                        VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;
                        VUart1Info.StepRcv.step = VCOM_STEP_START;
                        VUart1Info.StepRcv.freq = 0;
                        VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                    }
                    break;
                }
                case VCOM_STEP_STOP:
                {
                    if (LL_GPIO_IsOutputPinSet(VIRTUAL_UART1_RX_GPIOx, VIRTUAL_UART1_RX_PIN) == 1)
                    {
                        VUart1Info.StepRcv.counter++;
                        if (VUart1Info.StepRcv.counter >= VUart1Info.Config.stop)     /* 注意：这里实际丢了半个停止位 */
                        {
                            VUart1Info.StepRcv.counter = 0;
                            VUart1Info.Rx_WR_Index++;
                            if (VUart1Info.Rx_WR_Index >= VIRTUAL_UART1_RX_BUF_SIZE)
                            {
                                VUart1Info.Rx_WR_Index = 0;
                            }
                            VUart1Info.StepRcv.step = VCOM_STEP_START;
                            VUart1Info.StepRcv.freq = 0;
                            VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                        }
                        else
                        {
                            VUart1Info.StepRcv.freq++;
                        }
                    }
                    else
                    {
                        VUart1Info.Rx_Buf[VUart1Info.Rx_WR_Index] = 0x00;   /* 停止位错误，清除之前收到的bit */
                        VUart1Info.StepRcv.step = VCOM_STEP_START;
                        VUart1Info.StepRcv.freq = 0;
                        VirtualUartRxStart((UART_TYPE)VIRTUAL_UART1);
                    }
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        else
        {
            VUart1Info.StepRcv.freq++;
        }
    }
    #endif
}

/** 针对虚拟串口1的定时器中断处理函数
 *
 * @param[in] dir       接收还是发送
 * @retval void
**/
void VirtualUart1TXTimerIrqHandler(void)
{
    unsigned char byte;

    #if VIRTUAL_UART1_MODE == 1
    if (VUart1Info.dir == VCOM_DIR_TX)
    {
        if (LL_TIM_IsActiveFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER, TIM_IT_Update) != 0)
        {
            LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER);
            if (VUart1Info.Tx_RD_Index != VUart1Info.Tx_WR_Index)
            {
                switch (VUart1Info.StepSnd.step)
                {
                    case VCOM_STEP_START:
                    {
                        LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        VUart1Info.StepSnd.step = VCOM_STEP_DATA;
                        VUart1Info.StepSnd.bit1Num = 0;
                        VUart1Info.StepSnd.counter = 0;
                        // VUart1Info.StepSnd.freq++;
                        break;
                    }
                    case VCOM_STEP_DATA:
                    {
                        byte = VUart1Info.Tx_Buf[VUart1Info.Tx_RD_Index] >> VUart1Info.StepSnd.counter;
                        if (byte & 0x01)
                        {
                            LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                            VUart1Info.StepSnd.bit1Num++;
                        }
                        else
                        {
                            LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        }
                        VUart1Info.StepSnd.counter++;
                        if (VUart1Info.StepSnd.counter >= VUart1Info.Config.data)
                        {
                            if (VUart1Info.Config.parity != UART_PARITY_BIT_NULL)
                            {
                                VUart1Info.StepSnd.step = VCOM_STEP_PARITY;
                            }
                            else
                            {
                                VUart1Info.StepSnd.step = VCOM_STEP_STOP;
                            }
                            VUart1Info.StepSnd.counter = 0;
                        }
                        // VUart1Info.StepSnd.freq++;
                        break;
                    }
                    case VCOM_STEP_PARITY:
                    {
                        if ((VUart1Info.Config.parity == UART_PARITY_BIT_ODD && VUart1Info.StepSnd.bit1Num%2 == 0)
                        || (VUart1Info.Config.parity == UART_PARITY_BIT_EVEN && VUart1Info.StepSnd.bit1Num%2 != 0))
                        {
                            LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        }
                        else
                        {
                            LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        }
                        VUart1Info.StepSnd.step = VCOM_STEP_STOP;
                        // VUart1Info.StepSnd.freq++;
                        break;
                    }
                    case VCOM_STEP_STOP:
                    {
                        LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        VUart1Info.StepSnd.counter++;
                        if (VUart1Info.StepSnd.counter > VUart1Info.Config.stop)
                        {
                            VUart1Info.Tx_RD_Index++;
                            if (VUart1Info.Tx_RD_Index >= VIRTUAL_UART1_TX_BUF_SIZE)
                            {
                                VUart1Info.Tx_RD_Index = 0;
                            }
                            // VUart1Info.StepSnd.freq = 0;
                            VUart1Info.StepSnd.step = VCOM_STEP_START;
                        }
                        // else
                        // {
                        //     VUart1Info.StepSnd.freq++;
                        // }
                        break;
                    }
                    default:
                    {
                        VUart1Info.StepSnd.step = VCOM_STEP_START;
                        // VUart1Info.StepSnd.freq = 0;
                        break;
                    }
                }
            }
            else
            {
                VIRTUAL_UART1_TX_RX_TIMER->CR1 &= ~TIM_CR1_CEN;
                VIRTUAL_UART1_TX_RX_TIMER->ARR = VUart1Info.RcvPeriod;
                LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_RX_TIMER);
                // VUart1Info.StepSnd.freq = 0;
                VUart1Info.dir = VCOM_DIR_RX;
            }
            // if (VUart1Info.StepSnd.freq % 2 == 0)
            // {
            // }
            // else
            // {
            //     VUart1Info.StepSnd.freq++;
            // }
        }
    }
    #endif

    #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 2
    if (LL_TIM_IsActiveFlag_UPDATE(VIRTUAL_UART1_TX_TIMER) != 0)
    {
        LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_TIMER);
        if (VUart1Info.Tx_RD_Index != VUart1Info.Tx_WR_Index)
        {
            switch (VUart1Info.StepSnd.step)
            {
                case VCOM_STEP_START:
                {
                    LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                    VUart1Info.StepSnd.step = VCOM_STEP_DATA;
                    VUart1Info.StepSnd.bit1Num = 0;
                    VUart1Info.StepSnd.counter = 0;
                    // VUart1Info.StepSnd.freq++;
                    break;
                }
                case VCOM_STEP_DATA:
                {
                    byte = VUart1Info.Tx_Buf[VUart1Info.Tx_RD_Index] >> VUart1Info.StepSnd.counter;
                    if (byte & 0x01)
                    {
                        LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                        VUart1Info.StepSnd.bit1Num++;
                    }
                    else
                    {
                        LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                    }
                    VUart1Info.StepSnd.counter++;
                    if (VUart1Info.StepSnd.counter >= VUart1Info.Config.data)
                    {
                        if (VUart1Info.Config.parity != UART_PARITY_BIT_NULL)
                        {
                            VUart1Info.StepSnd.step = VCOM_STEP_PARITY;
                        }
                        else
                        {
                            VUart1Info.StepSnd.step = VCOM_STEP_STOP;
                        }
                        VUart1Info.StepSnd.counter = 0;
                    }
                    // VUart1Info.StepSnd.freq++;
                    break;
                }
                case VCOM_STEP_PARITY:
                {
                    if ((VUart1Info.Config.parity == UART_PARITY_BIT_ODD && VUart1Info.StepSnd.bit1Num%2 == 0)
                    || (VUart1Info.Config.parity == UART_PARITY_BIT_EVEN && VUart1Info.StepSnd.bit1Num%2 != 0))
                    {
                        LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                    }
                    else
                    {
                        LL_GPIO_ResetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                    }
                    VUart1Info.StepSnd.step = VCOM_STEP_STOP;
                    // VUart1Info.StepSnd.freq++;
                    break;
                }
                case VCOM_STEP_STOP:
                {
                    LL_GPIO_SetOutputPin(VIRTUAL_UART1_TX_GPIOx, VIRTUAL_UART1_TX_PIN);
                    VUart1Info.StepSnd.counter++;
                    if (VUart1Info.StepSnd.counter > VUart1Info.Config.stop)
                    {
                        VUart1Info.Tx_RD_Index++;
                        if (VUart1Info.Tx_RD_Index >= VIRTUAL_UART1_TX_BUF_SIZE)
                        {
                            VUart1Info.Tx_RD_Index = 0;
                        }
                        // VUart1Info.StepSnd.freq = 0;
                        VUart1Info.StepSnd.step = VCOM_STEP_START;
                    }
                    // else
                    // {
                    //     VUart1Info.StepSnd.freq++;
                    // }
                    break;
                }
                default:
                {
                    VUart1Info.StepSnd.step = VCOM_STEP_START;
                    // VUart1Info.StepSnd.freq = 0;
                    break;
                }
            }
        }
        else
        {
            LL_TIM_DisableCounter(VIRTUAL_UART1_TX_TIMER);   /* 关闭 */
            LL_TIM_ClearFlag_UPDATE(VIRTUAL_UART1_TX_TIMER);
            // VUart1Info.StepSnd.freq = 0;
        }
        // if (VUart1Info.StepSnd.freq % 2 == 0)
        // {
        // }
        // else
        // {
        //     VUart1Info.StepSnd.freq++;
        // }
    }
    #endif
}

/**
  * @brief  This function handles EXTI6 exception.
  * @param  None
  * @retval None
  */
void EXTI9_5_IRQHandler(void)
{
    VirtualUartExtiIrqHandler((UART_TYPE)VIRTUAL_UART1);
}

/**
  * @brief  This function handles TIM2 exception.
  * @param  None
  * @retval None
  */
void TIM2_IRQHandler(void)
{
    VirtualUart1TXTimerIrqHandler();
}

/**
  * @brief  This function handles TIM3 exception.
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(void)
{
    VirtualUart1RXTimerIrqHandler();
}

#endif

/** 通信口初始化
 *
 * @param[in] eCom      指定的通信口
 * @param[in] baud      波特率
 * @param[in] dataBit   数据位
 * @param[in] parity    校验位
 * @param[in] stop      停止位
 * @retval NULL
**/
void VirtualUartInit(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop)
{
    /* 引脚 */
    VirtualUartGPIOConfig(eCom);

    /* 参数 */
    #if VIRTUAL_UART1 == 1
    if (eCom == VIRTUAL_UART1)
    {
        VUart1Info.Config.parity = parity;
        VUart1Info.Config.data = dataBit;
        VUart1Info.Config.stop = stop;
        #if VIRTUAL_UART1_MODE == 1
        VUart1Info.dir = VCOM_DIR_RX;
        #endif
    }
    #endif
    /* 定时器 */
    VirtualUartTimerConfig(eCom, baud);

    /* 开始接收（外部中断模式接收） */
    VirtualUartRxStart(eCom);
}

/** 通信口失能
 *
 * @param[in] eCom      指定的通信口
 * @retval NULL
**/
void VirtualUartDeInit(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1
    if (eCom == VIRTUAL_UART1)
    {
        NVIC_DisableIRQ(VIRTUAL_UART1_EXTI_IRQ);
        #if VIRTUAL_UART1_MODE == 1
        LL_TIM_DeInit(VIRTUAL_UART1_TX_RX_TIMER);
        NVIC_DisableIRQ(VIRTUAL_UART1_TX_RX_TIMER_IRQ);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 2
        LL_TIM_DeInit(VIRTUAL_UART1_TX_TIMER);
        NVIC_DisableIRQ(VIRTUAL_UART1_TX_TIMER_IRQ);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3
        LL_TIM_DeInit(VIRTUAL_UART1_RX_TIMER);
        NVIC_DisableIRQ(VIRTUAL_UART1_RX_TIMER_IRQ);
        #endif
    }
    #endif
}

/**
  * @}
  */

/** @defgroup Uart read and write
 *  @brief   串口读写
 *  
@verbatim   
 ===============================================================================
               ##### Uart read and write #####
 ===============================================================================
    [..] 
    [..] 
         (#) 
@endverbatim
  * @{
  */

/** 串口发送数据
 *
 * @param[in] eCom      指定的通信口
 * @param[in] pData     要发送的数据
 * @param[in] len       要发送的数据长度
 * @retval void
**/
void VirtualUartSendData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    if (pData == NULL || len == 0)
    {
        return;
    }

    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2)
    if (eCom == VIRTUAL_UART1)
    {
        while ((VIRTUAL_UART1_TX_BUF_SIZE-(VIRTUAL_UART1_TX_BUF_SIZE + VUart1Info.Tx_WR_Index - VUart1Info.Tx_RD_Index)%VIRTUAL_UART1_TX_BUF_SIZE) < len)  /* 直到空间足够 */
        {
            VIRTUAL_UART_FEED_WEG();
        }
        for (int i = 0; i < len; ++i)
        {
            VUart1Info.Tx_Buf[VUart1Info.Tx_WR_Index++] = pData[i];
            if (VUart1Info.Tx_WR_Index >= VIRTUAL_UART1_TX_BUF_SIZE)
            {
                VUart1Info.Tx_WR_Index = 0;
            }
        }
        #if VIRTUAL_UART1_MODE == 1
        VUart1Info.dir = VCOM_DIR_TX;
        LL_TIM_DisableCounter(VIRTUAL_UART1_TX_TIMER);
        LL_TIM_SetAutoReload(VIRTUAL_UART1_TX_TIMER, VUart1Info.SndPeriod);
        LL_TIM_EnableCounter(VIRTUAL_UART1_TX_TIMER);
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 2
        LL_TIM_EnableCounter(VIRTUAL_UART1_TX_TIMER);
        #endif
    }
    #endif
}

/** 串口获取未读数据个数（单位字节）
 *
 * @param[in] eCom      指定的通信口
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int VirtualUartGetBytesNum(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3)
    if (eCom == VIRTUAL_UART1)
    {
        return (VIRTUAL_UART1_RX_BUF_SIZE + VUart1Info.Rx_WR_Index - VUart1Info.Rx_RD_Index)%VIRTUAL_UART1_RX_BUF_SIZE;
    }
    #endif
    return 0;
}

/** 串口获取数据
 *
 * @param[in] eCom      指定的通信口
 * @param[out] pData    存放数据
 * @param[in] len       要获取的数据长度
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int VirtualUartGetData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    int i = 0;

    if (len == 0 || pData == NULL || len > VirtualUartGetBytesNum(eCom))
    {
        return 0;
    }
    #if VIRTUAL_UART1 == 1 && (VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3)
    if (eCom == VIRTUAL_UART1)
    {
        if (VUart1Info.Rx_RD_Index == VUart1Info.Rx_WR_Index)
        {
            return 0;
        }
        else
        {
            for (i = 0; i < len; i++)
            {
                pData[i] = VUart1Info.Rx_Buf[VUart1Info.Rx_RD_Index++];
                if (VUart1Info.Rx_RD_Index == VIRTUAL_UART1_RX_BUF_SIZE)        /* 到达缓冲区末尾 */
                {
                    VUart1Info.Rx_RD_Index = 0;
                }
            }
        }
    }
    #endif

    return i;
}

/** 清空接收缓冲区
 *
 * @param[in] eCom        通信口
 * @retval void
**/
void VirtualUartBufClear(UART_TYPE eCom)
{
    #if VIRTUAL_UART1 == 1
    if (eCom == VIRTUAL_UART1)
    {
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3
        memset(VUart1Info.Rx_Buf, 0x00, VIRTUAL_UART1_RX_BUF_SIZE);
        VUart1Info.Rx_RD_Index = VUart1Info.Rx_WR_Index;
        #endif
        #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2
        VUart1Info.Tx_RD_Index = VUart1Info.Tx_WR_Index;
        #endif
    }
    #endif
}
#endif
/**
  * @}
  */


/************************ (C) COPYRIGHT iESLab *****END OF FILE****/
