/**
  ******************************************************************************
  * @file    ConfigDrivers.h
  * @author  ZCShou
  * @version 2.0.0
  * @date    2020.6.6
  * @brief   驱动库（含模块层接口文件）的配置文件
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2018 ZCShou </center></h2>
  *
  * 1. 该配置文件中需要引入自己的各种文件，才能实现某些配置。例如地址的定义可能是用户参数。从前往后分为 3 部分
  *     (+) 通用外设（串口、SPI等）
  *     (+) 外接芯片（RTC、FLASH等）
  *     (+) 模块层的配置（载波模块、485模块等）
  *
  * 2. 定时器 TIM 分配：
  *     （1）TIM2：用来实现延时函数
  *     （2）TIM3：ADC
  *     （3）TIM14：气体传感器
  *
  * 3. DMA 分配：
  *     （1）DMA1_Stream5 和 DMA1_Stream4：USART1 的收和发
  *     （2）DMA1_Stream3 和 DMA1_Stream2：USART3 的收和发
  *     （2）DMA1_Stream6 和 DMA1_Stream7：USART2 的收和发
  *     （3）DMA1_Stream1：ADC
  *
  ******************************************************************************
  */
#ifndef __CONFIG_DRIVERS_H__
#define __CONFIG_DRIVERS_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DriversDef.h"
#include "main.h"
#include "stm32f7xx.h"
#include "stm32f7xx_ll_system.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_bus.h"
#include "stm32f7xx_ll_utils.h"
#include "stm32f7xx_ll_gpio.h"
#include "stm32f7xx_it.h"

/* 如果更改了该文件则，必须同时编译/发布 IAP 和 APP */
// #warning "If you edited this file, please compile APP and IAP at the same time!"
/* -----------------------------------第一部分：看门口的配置选项----------------------------------- */
/* 选STM32 内部 IWDG。为 1 表示启用 */
#define WDG_STM32F7IWDG                             1
/* 配置项：IWDG 的时钟分频系数。
注意：IWDG 使用的时钟为 MCU 内部的 LSI（32KMz）。分频不同，最大超时时间不同，如下
*| Prescaler divider | PR[2:0] bits | Min timeout RL[11:0]= 0x000 | Max timeout RL[11:0]= 0xFFF |
*        /4                0                   0.125                           512
*        /8                1                   0.25                            1024
*        /16               2                   0.5                             2048
*        /32               3                   1                               4096
*        /64               4                   2                               8192
*        /128              5                   4                               16384
*        /256              6                                                   32768
*/
#define STM32F7IWDG_CLK_PRESCALER                   LL_IWDG_PRESCALER_64
/* 配置项：喂狗周期 T = (4*2^prv)* rlv / LSI。prv：预分频寄存器值；rlv：重装值，LSI 取 32（单位 KHZ）。单位毫秒。 */
#define STM32F7IWDG_FEED_CYCLE                      5000


/* -----------------------------------第二部分：延时定时器配置选项----------------------------------- */
/* 配置项：是否启用定时器功能。1：启用， 其他：关闭 */
#define DELAY_TIMERS_ENABLE                         1

#define DELAY_TIMERS_TIMER                          TIM1
#define DELAY_TIMERS_TIMER_CLK_ENABLE()             LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM1);
#define DELAY_TIMERS_TIMER_APB                      2   /* 定义 1：定时器位于 APB1； 2： APB2，以此来计算定时器频率 */
/* 最大延时（根据定时器不同而变化）*/
#define DELAY_TIMERS_DELAY_MAX                      0xFFFFF
// /* 喂狗宏：在启用开门狗时，由于Flash操作耗时较长，需要喂狗操作，只需要重定义该宏即可, 默认或不需要时该宏留空即可 */
// #define DELAY_TIMERS_FEED_WEG()                     WDG_Feed()


/* -----------------------------------第三部分：串口/模拟串口配置选项----------------------------------- */
/*=============== MCU 内部串口 ================*/
#define USART1_USED                                 1
#define USART2_USED                                 1
#define USART3_USED                                 1
#define UART4_USED                                  1
#define UART5_USED                                  1

#define USART_FEED_WEG()

#if USART1_USED == 1
    /* 接收时的工作模式：0 表示 DMA（默认），1 表示中断模式，2 表示轮询 */
    #define USART1_RX_MODE                          0
    /* 发送时的工作模式：0 表示 DMA（默认），1 表示中断模式，2 表示轮询 */
    #define USART1_TX_MODE                          0

    /* 接收缓冲区大小 */
    #define USART1_RX_BUF_SIZE                      512
    /* 发送缓冲区大小 */
    #define USART1_TX_BUF_SIZE                      512

    /* 时钟、中断 */
    #define USART1_CLK_ENABLE()                     LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1)
    #define USART1_CLK_DISABLE()                    LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_USART1)
    #define USART1_IRQ                              USART1_IRQn

    /* 引脚 */
    #define USART1_TX_PIN                           LL_GPIO_PIN_9
    #define USART1_TX_GPIOx                         GPIOA
    #define USART1_TX_GPIO_CLK                      LL_AHB1_GRP1_PERIPH_GPIOA
    #define USART1_RX_PIN                           LL_GPIO_PIN_10
    #define USART1_RX_GPIOx                         GPIOA
    #define USART1_RX_GPIO_CLK                      LL_AHB1_GRP1_PERIPH_GPIOA
    #define USART1_GPIO_CLK_ENABLE()                LL_AHB1_GRP1_EnableClock(USART1_TX_GPIO_CLK | USART1_RX_GPIO_CLK)

    /* DMA */
    #define USART1_DMA                              DMA2
    #define USART1_DMA_RX_CHANNEL                   LL_DMA_CHANNEL_4
    #define USART1_DMA_RX_STREAM                    LL_DMA_STREAM_2
    #define USART1_DMA_RX_IRQn                      DMA2_Stream2_IRQn
    #define USART1_DMA_TX_CHANNEL                   LL_DMA_CHANNEL_4
    #define USART1_DMA_TX_STREAM                    LL_DMA_STREAM_7
    #define USART1_DMA_TX_IRQn                      DMA2_Stream7_IRQn
    #define USART1_DMA_CLK_ENABLE()                 LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2)
#endif
/*=============== 模拟串口 ================*/
// /* 模拟串口1 */
// #define VIRTUAL_UART1                               1   /* 定义为 1 表示启用，其他任意值 表示不启用 */
// #if VIRTUAL_UART1 == 1
//     /* 模式：0：全双工（收发需要不同的定时器），1 半双工（收发共用一个定时器），2 仅发送（只需要一个定时器），3 仅接收（只需要一个定时器） */
//     #define VIRTUAL_UART1_MODE                      0
//     /* 缓冲区大小 */
//     #define VIRTUAL_UART1_RX_BUF_SIZE               512
//     #define VIRTUAL_UART1_TX_BUF_SIZE               512
//     /* IO */
//     #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 2
//     #define VIRTUAL_UART1_TX_PIN                    LL_GPIO_PIN_5
//     #define VIRTUAL_UART1_TX_GPIOx                  GPIOA
//     #define VIRTUAL_UART1_TX_GPIO_SOURCE            GPIO_PinSource5
//     #define VIRTUAL_UART1_TX_GPIO_CLK               LL_AHB1_GRP1_PERIPH_GPIOA
//     #endif
//     #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 1 || VIRTUAL_UART1_MODE == 3
//     #define VIRTUAL_UART1_RX_PIN                    LL_GPIO_PIN_6
//     #define VIRTUAL_UART1_RX_GPIOx                  GPIOA
//     #define VIRTUAL_UART1_RX_GPIO_SOURCE            GPIO_PinSource6
//     #define VIRTUAL_UART1_RX_GPIO_CLK               LL_AHB1_GRP1_PERIPH_GPIOA
//     /* 接收引脚的外部中断 */
//     #define VIRTUAL_UART1_EXTI_Config()             LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE6)
//     #define VIRTUAL_UART1_EXTI_Line                 LL_EXTI_LINE_6
//     #define VIRTUAL_UART1_EXTI_IRQ                  EXTI9_5_IRQn
//     // #define VIRTUAL_UART1_EXTI_CLK_ENABLE()         LL_APB2_GRP1_EnableClock(VIRTUAL_UART1_TX_GPIO_CLK|VIRTUAL_UART1_RX_GPIO_CLK)
//     #endif
//     #define VIRTUAL_UART1_GPIO_CLK_ENABLE()         LL_APB2_GRP1_EnableClock(VIRTUAL_UART1_TX_GPIO_CLK|VIRTUAL_UART1_RX_GPIO_CLK)
//     /* 定时器 */
//     #if VIRTUAL_UART1_MODE == 1     /* 此时收发共用一个定时器 */
//     #define VIRTUAL_UART1_TX_RX_TIMER               TIM2
//     #define VIRTUAL_UART1_TX_RX_TIMER_CLK_ENABLE()  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2)
//     #define VIRTUAL_UART1_TX_RX_TIMER_IRQ           TIM2_IRQn
//     #define VIRTUAL_UART1_TX_RX_TIMER_Prescaler     0
//     #define VIRTUAL_UART1_TX_RX_TIMER_APB           1   /* 定义 1：定时器位于 APB1； 2： APB2，以此来计算定时器频率 */
//     #endif
//     #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 2      /* 发送定时器 */
//     #define VIRTUAL_UART1_TX_TIMER                  TIM2
//     #define VIRTUAL_UART1_TX_TIMER_CLK_ENABLE()     LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2)
//     #define VIRTUAL_UART1_TX_TIMER_IRQ              TIM2_IRQn
//     #define VIRTUAL_UART1_TX_TIMER_Prescaler        0
//     #define VIRTUAL_UART1_TX_TIMER_APB              1   /* 定义 1：定时器位于 APB1； 2： APB2，以此来计算定时器频率 */
//     #endif
//     #if VIRTUAL_UART1_MODE == 0 || VIRTUAL_UART1_MODE == 3      /* 接收定时器 */
//     #define VIRTUAL_UART1_RX_TIMER                  TIM3
//     #define VIRTUAL_UART1_RX_TIMER_CLK_ENABLE()     LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3)
//     #define VIRTUAL_UART1_RX_TIMER_IRQ              TIM3_IRQn
//     #define VIRTUAL_UART1_RX_TIMER_Prescaler        0
//     #define VIRTUAL_UART1_RX_TIMER_APB              1   /* 定义 1：定时器位于 APB1； 2： APB2，以此来计算定时器频率 */
//     #endif
// #endif


/*============= 串口功能映射 ==============*/
/**
 * @brief 自定义通信口与串口映射
 */
#if USART1_USED == 1
typedef enum
{
    #if USART1_USED == 1
    UART_COMM = (int)USART1,       /* COMM 使用 串口 1 */
    #endif
} UART_TYPE;
#endif


/* -----------------------------------第四部分：SPI配置选项----------------------------------- */

/* -----------------------------------第五部分：I2C 及模拟 I2C 配置选项----------------------------------- */

/* -----------------------------------第六部分：终端指示灯引脚配置选项----------------------------------- */

/* -----------------------------------第七部分：Flash驱动库配置选项----------------------------------- */

/* -----------------------------------第八部分：RTC 配置选项----------------------------------- */

/* -----------------------------------第九部分：ADC配置选项----------------------------------- */

/* -----------------------------------第十部分：NET4G 模块的配置选项----------------------------------- */

/* -----------------------------------第十一部分：LCD 驱动芯片的配置选项----------------------------------- */

/* -----------------------------------第十二部分：红外配置选项----------------------------------- */

/* -----------------------------------第十三部分：RS485配置选项----------------------------------- */

/* -----------------------------------第十四部分：蓝牙配置选项----------------------------------- */

/* -----------------------------------第十五部分：通用IO配置选项----------------------------------- */


#ifdef __cplusplus
}
#endif

#endif /* __CONFIG_DRIVERS_H__ */

