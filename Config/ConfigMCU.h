/**
  ******************************************************************************
  * @file    ConfigMCU.h
  * @author  ZCShou
  * @version 2.0.0
  * @date    2020.6.6
  * @brief   MCU 内部的 FLash 空间划分
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2018 ZCShou </center></h2>
  *
  * 1. 不带 IAP 的项目模板！
  *
  * 2. 整个程序的基本组成结构（通用性设计，目前以下的程序设计理论上适合所有ST芯片，只需要根据产品修改各宏值即可）
  *        (+) APP：主应用程序，正常的功能操作均在此。
  *        (+) PRAM：在 Flash 的一块独立存储空间，用来存放各种参数。
  *        注意：
  *         (1) 可能需要根据芯片的FLash（扇区大小不一致），调整参数区和APP的位置
  *
  * 3. APP 结构为：ARM中断向量表 + 自己添加的程序信息 + ARM 程序代码。
  *     配合分散加载文件实现！配合分散加载文件实现！配合分散加载文件实现！
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONFIG_MCU_H__
#define __CONFIG_MCU_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx.h"
/* Exported define -----------------------------------------------------------*/
/* -------------------------程序由 APP、PRAM 两部分组成（可能需要根据芯片的FLash（扇区大小不一致），调整PRAM和APP的位置）----------------------- */
#define MCU_SRAM_SPACE_SIZE                 0xC000                      /* SRAM 总大小 48 KB */
#define MCU_FLASH_SPACE_SIZE                0x40000                     /* Flash 总大小 256KB */
/* APP地址及空间大小 */
#define MCU_FLASH_APP_SPACE_ADDR            0x8000000
#define MCU_FLASH_APP_SPACE_SIZE            (MCU_FLASH_SPACE_SIZE - MCU_FLASH_PARM_SPACE_SIZE)
/* 参数区地址及空间大小（位置取决于 MCU FLASH 结构）*/
#define MCU_FLASH_PARM_SPACE_ADDR           (0x8000000 + MCU_FLASH_SPACE_SIZE - MCU_FLASH_PARM_SPACE_SIZE)
#define MCU_FLASH_PARM_SPACE_SIZE           0x1000                    /* 4KB */

/* -------------------------应用程序信息宏值。必须是4的倍数（为了产品中统一，最好不要修改以下值，不能存放时考虑简写）----------------------- */
#define FACTORY_LENGTH                      16
#define PRODUCT_LENGTH                      16
#define PROTOCOL_LENGTH                     16
#define VER_SW_LENGTH                       16
#define VER_HW_LENGTH                       16
#define DATE_SW_LENGTH                      16
#define DATE_HW_LENGTH                      16
/* Exported types ------------------------------------------------------------*/
/**
  * @brief 应用程序信息(必须为4字节对齐)
  */
typedef struct APP_INFO
{
    char ucFactory[FACTORY_LENGTH];         /* 厂商标志 */
    char ucProduct[PRODUCT_LENGTH];         /* 产品标志 */
    char ucProtocol[PROTOCOL_LENGTH];       /* 规约标志 */
    char ucVerSW[VER_SW_LENGTH];            /* 软件版本 */
    char ucVerDateSW[DATE_SW_LENGTH];       /* 软件发布日期 */
    char ucVerHW[VER_SW_LENGTH];            /* 硬件版本 */
    char ucVerDateHW[DATE_HW_LENGTH];       /* 硬件发布日期 */
} APP_INFO;
/* Exported constants --------------------------------------------------------*/
#if (defined(__CC_ARM))
    extern unsigned int __Vectors_Size;     /* 导入的 .s中的中断向量表的大小。仅针对 ARM 的编译器。IAR 和 Gcc for ARM 不可用 */
    #define MCU_VECTORS_SIZE                (unsigned int)&__Vectors_Size
#elif (defined(__ICCARM__))
    /* IAR 的启动文件中没有中断向量表的大小。因此只能根据 MCU 自己定义 */
    #define MCU_VECTORS_SIZE                304
#else
    #define MCU_VECTORS_SIZE                304
#endif
#define MCU_FLASH_APP_INFO_ADDR         (MCU_FLASH_APP_SPACE_ADDR + MCU_VECTORS_SIZE)        /* 位于中断向量表之后 */
/* Exported variables --------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __CONFIG_MCU_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
