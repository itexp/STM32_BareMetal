# STM32 BareMetal
本项目是一个 STM32 裸机项目模板。该项目是根据多年嵌入式开发经验总结出的一套 STM32 裸机程序代码目录结构及架构，从 IAP 到 APP 都经过了精心的设计。

## APP 架构
![STM32Framework_APP](./Docs/STM32Framework_APP.png)

## IAP 架构
![STM32Framework_IAP](./Docs/STM32Framework_IAP.png)

## 驱动架构
![STM32Framework_driver](./Docs/STM32Framework_driver.png)

# 开发工具
本 STM32 裸机项目模板中配置好了常用开发工具的工程文件，方便选择合适的开发工具，所有开发工具编译均无错误，无警告。

## MDK-ARM
TBD

## ECLIPSE
1. 本模板中，将与开发环境相关的文件（例如，启动文件 `xx.S`、连接脚本文件）放到了 IDE 项目文件所在目录，而 ECLIPSE 会自动把项目所在目录下的文件添加到项目中，如果手动添加会导致重复
2. ST 提供的启动文件是 `xxx.s`（小写），而 ECLIPSE 默认把 `.S`（大写） 当做汇编，这会导致 ECLIPSE 不识别启动文件，因此需要手动更改启动文件名为 `.S`

## SEGGER-ES
TBD

## EWARM
TBD
