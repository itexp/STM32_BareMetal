/**
  ******************************************************************************
  * @file    UART.h
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   UART 驱动模块接口文件，用来统一处理多种UART芯片
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1)
  *
  *  (2)
  *
  *  (3)
  *
  ******************************************************************************
  */
#ifndef __UART_H__
#define __UART_H__

#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "UARTDef.h"
/* Exported defines ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported variables ------------------------------------------------------- */
/* Exported functions ------------------------------------------------------- */
bool UART_Init(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop);

bool UART_CustomInit(UART_TYPE eCom, UART_BAUDRATE_INDEX_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop);

void UART_DeInit(UART_TYPE eCom);

void UART_SendData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

unsigned int UART_GetRcvBytesNum(UART_TYPE eCom);

unsigned int UART_GetData(UART_TYPE eCom, unsigned char* pData, unsigned int len);

void UART_ClearBuffer(UART_TYPE eCom);

#ifdef __cplusplus
}
#endif

#endif /* __UART_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
