/**
  ******************************************************************************
  * @file    Delay.h
  * @author  赵长收
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   Delay 模块，用来统一处理多种Delay芯片
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2019 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
#ifndef __DELAY_H__
#define __DELAY_H__

#ifdef __cplusplus
 extern "C" { 
#endif
/* Includes ------------------------------------------------------------------*/
#include "ConfigDrivers.h"
/* Exported defines ----------------------------------------------------------*/
/*
 * 延时用的定时器
*/
typedef struct DelayTimer
{
    volatile bool isDelay;
    volatile unsigned int Counter;
} DelayTimer;
/* Exported types ------------------------------------------------------------*/
/* Exported variables ------------------------------------------------------- */
/* Exported functions ------------------------------------------------------- */
bool DelayInit(void);

void DelayUS(unsigned int us);

bool DelayMS(unsigned int ms);

void DelayCounterProcess(void);

#ifdef __cplusplus
}
#endif

#endif /* __Delay_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
