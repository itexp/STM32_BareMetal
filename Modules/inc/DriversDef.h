/**
  ******************************************************************************
  * @file    DriversDef.h
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   驱动 通用数据项的定义
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
#ifndef __DRIVERS_DEF_H__
#define __DRIVERS_DEF_H__

#ifdef __cplusplus
 extern "C" { 
#endif
/* Includes ------------------------------------------------------------------*/
/* Exported defines ----------------------------------------------------------*/
/**
 * @brief  返回值类型
 */
#define DRIVERS_RET_OK                              (0)     /*  */
#define DRIVERS_RET_DONE                            DRIVERS_RET_OK  /* 兼容使用 */
#define DRIVERS_RET_ERROR                           (-1)    /* 出错 */
#define DRIVERS_RET_DOING                           (-2)    /* 处理中（用于处理耗时较长的驱动接口，将时间交由上层调用者，避免死等） */
#define DRIVERS_RET_TIMEOUT                         (-3)    /* 超时 */
/* Exported types ------------------------------------------------------------*/
/* Exported variables ------------------------------------------------------- */
/* Exported macro ----------------------------------------------------------- */
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif /* __DRIVERS_DEF_H__ */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
