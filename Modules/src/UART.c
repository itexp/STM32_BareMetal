/**
  ******************************************************************************
  * @file    UART.c
  * @author  ZCShou
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   UART 驱动模块接口文件，用来统一处理多种UART芯片
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 ZCShou </center></h2>
  *
  *  (1)
  *
  *  (2)
  *
  *  (3)
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "UART_STM32F7_LL.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup UART
 *  @brief   UART
 *
@verbatim
 ===============================================================================
               ##### UART #####
 ===============================================================================
    [..] 配置
    [..]
@endverbatim
  * @{
  */

/** 初始化
 *
 * @param[in] eCom      指定的通信口
 * @param[in] baud      波特率
 * @param[in] dataBit   数据位
 * @param[in] parity    校验位
 * @param[in] stop      停止位
 * @retval 是否成功
**/
bool UART_Init(UART_TYPE eCom, UART_BAUDRATE_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop)
{
    return STM32F7UART_Init(eCom, baud, dataBit, parity, stop);
}

/** 初始化
 *
 * @param[in] eCom      指定的通信口
 * @param[in] baud      波特率
 * @param[in] dataBit   数据位
 * @param[in] parity    校验位
 * @param[in] stop      停止位
 * @retval 是否成功
**/
bool UART_CustomInit(UART_TYPE eCom, UART_BAUDRATE_INDEX_TYPE baud, UART_DATA_BIT_TYPE dataBit, UART_PARITY_BIT_TYPE parity, UART_STOP_BIT_TYPE stop)
{
    UART_BAUDRATE_TYPE bd;

    switch (baud)
    {
        case UART_BAUDRATE_INDEX_1200:
        {
            bd = UART_BAUDRATE_1200;
            break;
        }
        case UART_BAUDRATE_INDEX_2400:
        {
            bd = UART_BAUDRATE_2400;
            break;
        }
        case UART_BAUDRATE_INDEX_4800:
        {
            bd = UART_BAUDRATE_4800;
            break;
        }
        case UART_BAUDRATE_INDEX_9600:
        {
            bd = UART_BAUDRATE_9600;
            break;
        }
        case UART_BAUDRATE_INDEX_19200:
        {
            bd = UART_BAUDRATE_19200;
            break;
        }
        case UART_BAUDRATE_INDEX_38400:
        {
            bd = UART_BAUDRATE_38400;
            break;
        }
        case UART_BAUDRATE_INDEX_57600:
        {
            bd = UART_BAUDRATE_57600;
            break;
        }
        case UART_BAUDRATE_INDEX_115200:
        {
            bd = UART_BAUDRATE_115200;
            break;
        }
        default:
        {

            return false;
        }
    }

    return STM32F7UART_Init(eCom, bd, dataBit, parity, stop);
}

/** 失能
 *
 * @param[in] eCom      指定的通信口
 * @retval NULL
**/
void UART_DeInit(UART_TYPE eCom)
{
    STM32F7UART_DeInit(eCom);
}

/** 发送数据
 *
 * @param[in] eCom      指定的通信口
 * @param[in] pData     要发送的数据
 * @param[in] len       要发送的数据长度
 * @retval void
**/
void UART_SendData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    STM32F7UART_SendData(eCom, pData, len);
}

/** 获取未读数据个数（单位字节）
 *
 * @param[in] eCom      指定的通信口
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int UART_GetRcvBytesNum(UART_TYPE eCom)
{
    return STM32F7UART_GetBytesNum(eCom);
}

/** 获取数据
 *
 * @param[in] eCom      指定的通信口
 * @param[out] pData    存放数据
 * @param[in] len       要获取的数据长度
 * @retval 正常返回数据长度，失败或者无数据返回0
**/
unsigned int UART_GetData(UART_TYPE eCom, unsigned char* pData, unsigned int len)
{
    return STM32F7UART_GetData(eCom, pData, len);
}

/** 清空接收缓冲区
 *
 * @param[in] eCom        通信口
 * @retval void
**/
void UART_ClearBuffer(UART_TYPE eCom)
{
    STM32F7UART_ClearBuffer(eCom);
}

/**
  * @}
  */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
