/**
  ******************************************************************************
  * @file    Delay.c
  * @author  赵长收
  * @version 1.0.0
  * @date    2020.10.6
  * @brief   Delay 驱动模块接口文件，用来统一处理多种Delay芯片
  ******************************************************************************
  * @attention
  * 
  * <h2><center>&copy; COPYRIGHT 2019 ZCShou </center></h2>
  *
  *  (1) 
  *
  *  (2) 
  *
  *  (3) 
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "DelayTimers_STM32F7_LL.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/**
 * @brief  延时定时计数器
 */
static DelayTimer DelayCounter;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup Delay
 *  @brief   Delay
 * 
@verbatim   
 ===============================================================================
               ##### Delay #####
 ===============================================================================
    [..] 
    [..] 
@endverbatim
  * @{
  */

/** 初始化
 *
 * @param[in] void
 * @retval NULL
**/
bool DelayInit(void)
{
    return STM32F7TIMER_Init();
}

/** 微秒延时
 *
 * @param[in] us        微妙值（0 ~ 0XFFFF）
 * @retval 是否延时完成
**/
void DelayUS(unsigned int us)
{
    STM32F7TIMER_DelayUS(us);
}

/** 毫秒延时
 *
 * @param[in] ms        毫秒值（0 ~ 0XFFFFFFFF）
 * @retval 是否延时完成
**/
bool DelayMS(unsigned int ms)
{
    DelayCounter.isDelay = true;

    while(DelayCounter.Counter < ms)
    {
        //WDG_Feed();
    }
    DelayCounter.isDelay = false;
    DelayCounter.Counter = 0;
    return true;
}

/** 延时计数器
 *
 * @param[in] void
 * @retval void
**/
void DelayCounterProcess(void)
{
    if (DelayCounter.isDelay)
    {
        DelayCounter.Counter++;
    }
}

/**
  * @}
  */

/************************ (C) COPYRIGHT ZCShou *****END OF FILE****/
